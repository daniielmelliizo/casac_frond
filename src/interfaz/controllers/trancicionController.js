class TrancicionController {
    constructor(trancicionService) {
      this.trancicionService = trancicionService;
    }
    async handleCreateTrancicion(data) {
      try {
        const transicion = await this.trancicionService.createTrancicion(data);
        return transicion;
      } catch (error) {
        return error;
      }
    }

  
    async handleUpdateTrancicion(id, data) {
      try {
        const trancicion = await this.trancicionService.updateTrancicion(id, data);
        // console.log("Se pudo actualizar exitosamente", trancición);
        return trancicion;
      } catch (error) {
        // console.error("Error al actualizar usuario:", error.message);
        return false;
      }
    }
    async handleConsultar(id) {
      try {
        const result = await this.trancicionService.getTrancicionConsult(id);
        return result;
      } catch (error) {
        console.error("Error al encontrar la guía:", error.message);
        return false;
      }
    }
  }
  
  export default TrancicionController;
  