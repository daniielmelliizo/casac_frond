class GuiaController {
  constructor(guiaService) {
    this.guiaService = guiaService;
  }
  async handleCreateGuia(data) {
    // console.log(data);
    try {
      const guia = await this.guiaService.createGuia(data);
      // console.log("se pudo crear exictosamente", guia);
      return guia;
    } catch (error) {
      return false;
    }
  }

  async handleGetGuias() {
    try {
      const guias = await this.guiaService.getGuia();
      // console.log("Guias obtenidas exitosamente", guias);
      return guias;
    } catch (error) {
      console.error("Error al obtener guías:", error.message);
      return [];
    }
  }

  async handleUpdateGuia(id, data) {
    try {
      const guia = await this.guiaService.updateGuia(id, data);
      // console.log("Se pudo actualizar exitosamente", guia);
      return true;
    } catch (error) {
      console.error("Error al actualizar guía:", error.message);
      return false;
    }
  }

  async handleDeleteGuia(id) {
    try {
      const result = await this.guiaService.deleteGuia(id);
      // console.log("Guía eliminada exitosamente", result);
      return true;
    } catch (error) {
      console.error("Error al eliminar guía:", error.message);
      return false;
    }
  }
  async handleConsultar(id) {
    try {
      const result = await this.guiaService.getGuiaConsult(id);
      return result;
    } catch (error) {
      console.error("Error al encontrar la guía:", error.message);
      return false;
    }
  }

  async handleGetFormActivo() {
    try {
      const result = await this.guiaService.getFormActivo();
      // console.log("formulario activas obtenidas exitosamente", result);
      return result;
    } catch (error) {
      console.error("Error al obtener guías:", error.message);
      return [];
    }
  }

}

export default GuiaController;
