class UserController {
    constructor(userService) {
      this.userService = userService;
    }
    async handleCreateUser(data) {
      // console.log(data);
      try {
        const user = await this.userService.createUser(data);
        // console.log('se pudo crear exictosamente',user)
        return user.mensaje;
      } catch (error) {
        // console.log('no se pudo crear correctamente', error)
        return error;
      }
    }

    async handleGetUsers() {
      try {
        const users = await this.userService.getUsers();
        // console.log("usuarios obtenidas exitosamente", users);
        return users;
      } catch (error) {
        // console.error("Error al obtener guías:", error.message);
        return [];
      }
    }
  
    async handleUpdateUsers(id, data) {
      try {
        const users = await this.userService.updateUser(id, data);
        // console.log("Se pudo actualizar exitosamente", users);
        return users;
      } catch (error) {
        // console.error("Error al actualizar usuario:", error.message);
        return false;
      }
    }
  
    async handleDeleteUser(id) {
      try {
        const result = await this.userService.deleteUser(id);
        // console.log("usuario eliminada exitosamente", result);
        return result;
      } catch (error) {
        // console.error("Error al eliminar usuario:", error.message);
        return false;
      }
    }
    async handleParametricas(id) {
      try {
        const users = await this.userService.Parametricas(id);
        console.log("Parametricas funcionando", users);
        return users;
      } catch (error) {
        console.error("Error al obtener los formularios", error);
        return [];
      }
    }
  }
  
  export default UserController;
  