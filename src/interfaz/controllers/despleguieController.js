class DespliegueController {
    constructor(despliegueService) {
      this.despliegueService = despliegueService;
    }
    
    async handleUpdateDespliegue(data) {
      try {
        const despliegue = await this.despliegueService.updateDespliegue(data);
        // console.log("Se pudo desplegar exitosamente", despliegue);
        return true;
      } catch (error) {
        console.error("Error al desplegar las guias:", error.message);
        return false;
      }
    }

}
  export default DespliegueController;
  