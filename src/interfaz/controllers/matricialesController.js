class matricialesController {
    constructor(matricialesService) {
      this.matricialesService = matricialesService;
    }

    async handleGetMatricialesFinca() {
      try {
        const response = await this.matricialesService.getMatricialesFinca();
        // console.log("usuarios obtenidas exitosamente", users);
        return response;
      } catch (error) {
        // console.error("Error al obtener guías:", error.message);
        return [];
      }
    }
  
  }
  
  export default matricialesController;
  