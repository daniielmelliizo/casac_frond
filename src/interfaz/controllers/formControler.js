class FormController {
    constructor(formService) {
      this.formService = formService;
    }
    async handleCreateForm(data) {
      // console.log(data);
      try {
        const result = await this.formService.createForm(data);
        // console.log("se pudo crear exictosamente", result);
        return true;
      } catch (error) {
        return false;
      }
    }
  
    async handleGetForm() {
      try {
        const result = await this.formService.getForm();
        // console.log("formulario obtenidas exitosamente", result);
        return result;
      } catch (error) {
        // console.error("Error al obtener el formularios:", error.message);
        return [];
      }
    }
  
    async handleUpdateForm(id, data) {
      try {
        const result = await this.formService.updateForm(id, data);
        // console.log("Se pudo actualizar exitosamente", result);
        return true;
      } catch (error) {
        console.error("Error al actualizar el formulario:", error.message);
        return error;
      }
    }
  
    async handleDeleteForm(id) {
      try {
        const result = await this.formService.deleteForm(id);
        // console.log("Formulario eliminado exitosamente", result);
        return true;
      } catch (error) {
        // console.error("Error al eliminar formulario:", error.message);
        return false;
      }
    }
    async handleConsultarForm(id) {
      try {
        const result = await this.formService.getFormConsult(id);
        return result;
      } catch (error) {
        // console.error("Error al encontrar la guía:", error.message);
        return false;
      }
    }

  
  }
  
  export default FormController;
  