class AuthenticationController {
  constructor(authenticationService) {
    this.authenticationService = authenticationService;
  }
  async handleLogin(email, password) {
    try {
      const user = await this.authenticationService.login(email, password);
      localStorage.setItem("id", user.usuario.uid);
      localStorage.setItem("name", user.usuario.nombres);
      return true;
    } catch (error) {
      return false;
    }
  }
}

export default AuthenticationController;
