class SeccionesController {
    constructor(seccionService) {
      this.seccionService = seccionService;
    }
    async handleCreateSeccion(data) {
      // console.log(data);
      try {
        const guia = await this.seccionService.createSeccion(data);
        // console.log("se pudo crear exictosamente", guia);
        return true;
      } catch (error) {
        return false;
      }
    }
    async handleBuscarSeccion(data) {
      // console.log(data);
      try {
        const guia = await this.seccionService.buscarSeccion(data);
        // console.log("se pudo extraer exictosamente", guia);
        return guia;
      } catch (error) {
        return false;
      }
    }
    async handleModifySeccion(id,data) {
      try {
        const guia = await this.seccionService.updateSeccion(id,data);
        // console.log("se pudo actualizar exictosamente", guia);
        return true;
      } catch (error) {
        console.error("Error al actualizar seccion:", error.message);
        return false;
      }
    }
}
export default SeccionesController;