import React, { useState } from 'react';

const CheckboxGroup = ({ questionIndex, campo, onCheckboxChange }) => {
  const Checkbox = ({ label, initialValue = false }) => {
    const [isChecked, setChecked] = useState(initialValue);

    const handleCheckboxChange = () => {
      const newCheckedState = !isChecked;
      setChecked(newCheckedState);

      // Llamar a la función de devolución de llamada si se proporciona
      if (onCheckboxChange) {
        onCheckboxChange(questionIndex, label, newCheckedState);
      }
    };

    return (
      <label>
        <input
          className="input"
          type="checkbox"
          checked={isChecked}
          onChange={handleCheckboxChange}
        />
        {label}
      </label>
    );
  };

  return (
    <div key={questionIndex} className="form-question">
      {campo.opciones &&
        campo.opciones.map((opcion, index) => (
          <Checkbox
            key={index}
            label={opcion.etiqueta}
            initialValue={false}
          />
        ))}
    </div>
  );
};

export default CheckboxGroup;
