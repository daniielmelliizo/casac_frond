import React from 'react';
import PropTypes from 'prop-types';

const ProgressBar = ({ value, maxValue, color }) => {
  // Calcular el ancho de la barra de progreso
  const progressWidth = (value / maxValue) * 100;

  return (
    <div className="progress-bar-container" style={{ position: 'relative', width: '100%', height: '15px' }}>
      <div className="progress-bar-background" style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%', backgroundColor: '#ccc' }} />
      <div
        className="progress-bar"
        style={{ position: 'absolute', top: 0, left: 0, width: `${progressWidth}%`, height: '100%', backgroundColor: color }}
      />
    </div>
  );
};

ProgressBar.propTypes = {
  value: PropTypes.number.isRequired, // Valor actual de la barra de progreso
  maxValue: PropTypes.number.isRequired, // Valor máximo de la barra de progreso
  color: PropTypes.string.isRequired, // Color de la barra de progreso
};

export default ProgressBar;
