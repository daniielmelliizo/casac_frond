// UserModal.js

import React from 'react';
import Modal from "react-modal";
import "../../styles/Usermodalstyle.css"

const UserModal = ({
  isOpen,
  onRequestClose,
  contentLabel,
  formData,
  handleInputChange,
  handleInputChangeEditModal,
  handleEditModal,
  handleRegistro,
  handleCloseModal,
}) => {
  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      contentLabel={contentLabel}
      className="custom-modal"
    >
      <h2>{contentLabel}</h2>
      <form>
        <div className="form-container">
          <label htmlFor="nombres">Nombre</label>
          <input
            type="text"
            id="nombres"
            name="nombres"
            value={formData.nombres}
            onChange={handleInputChangeEditModal || handleInputChange}
            required
          />

          <label htmlFor="apellidos">Apellido</label>
          <input
            type="text"
            id="apellidos"
            name="apellidos"
            value={formData.apellidos}
            onChange={handleInputChangeEditModal || handleInputChange}
            required
          />

          <label htmlFor="identificacion">Identificación</label>
          <input
            type="number"
            id="identificacion"
            name="identificacion"
            value={formData.identificacion}
            onChange={handleInputChange || handleInputChangeEditModal}
            maxLength="10"
            minLength="6"
            required
          />

          <label htmlFor="universidad">Universidad</label>
          <input
            type="text"
            id="universidad"
            name="universidad"
            value={formData.universidad}
            onChange={handleInputChange || handleInputChangeEditModal}
            required
          />

          <label htmlFor="profesion">Profesión</label>
          <input
            type="text"
            id="profesion"
            name="profesion"
            value={formData.profesion}
            onChange={handleInputChange || handleInputChangeEditModal}
            required
          />

          <label htmlFor="grupo_investigacion">Grupo de Investigador</label>
          <input
            type="text"
            id="grupo_investigacion"
            name="grupo_investigacion"
            value={formData.grupo_investigacion}
            onChange={handleInputChange || handleInputChangeEditModal}
            required
          />

          <label htmlFor="rol">Rol</label>
          <select
            id="rol"
            name="rol"
            value={formData.rol}
            onChange={handleInputChange || handleInputChangeEditModal}
          >
            <option value="">Selecciona...</option>
            <option value="Investigador">Investigador</option>
            <option value="Promotor">Promotor</option>
            <option value="Productor">Productor</option>
          </select>

          <label htmlFor="correo">Correo</label>
          <input
            type="email"
            id="correo"
            name="correo"
            value={formData.correo}
            onChange={handleInputChange || handleInputChangeEditModal}
            required
          />

          {handleRegistro &&(
            <div className='exception'>
          <label htmlFor="contrasena">Contraseña</label>
          <input
            style={{ display: "flex", alignItems: "center" }}
            type="password"
            id="contrasena"
            name="contrasena"
            value={formData.contrasena}
            onChange={handleInputChange || handleInputChangeEditModal}
            required
            disabled={!handleRegistro}            
          />
          </div>)}
          </div>
          <div className="button-container">
          {contentLabel === 'Editar Usuario' ? (
            <button
              className="close-button"
              data-tooltip="Editar"
              type="button"
              onClick={() => handleEditModal(formData.id, formData)}
            >
              Editar
            </button>
          ) : (
            <button
              className="btns_usuarios"
              type="button"
              onClick={handleRegistro}
            >
              Guardar
            </button>
          )}

          <button
            className="btns_usuarios"
            data-tooltip="Cerrar"
            onClick={handleCloseModal}
          >
            Cerrar
          </button>
          </div>
      </form>
    </Modal>
  );
};

export default UserModal;
