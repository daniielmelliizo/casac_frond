import React from 'react';

const MatrixComponent = ({ campo, questionIndex }) => {
  const filas = campo.opciones.filas;
  const columnas = campo.opciones.columnas;

  if (!filas || !columnas) {
    return null;
  }

  return (
    <table key={questionIndex} className="matrix-table">
      <thead>
        <tr>
          <th></th>
          {columnas.map((columna, columnIndex) => (
            <th key={columnIndex}>{columna.etiqueta}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {filas.map((fila, filaIndex) => (
          <tr key={filaIndex}>
            <td>{fila.etiqueta}</td>
            {columnas.map((columna, columnIndex) => (
              <td key={columnIndex}>
                <input
                  type="radio"
                  name={`respuesta_${filaIndex}`}
                  id={`respuesta_${filaIndex}_${columnIndex}`}
                  value={`${fila.etiqueta} - ${columna.etiqueta}`}
                />
              </td>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default MatrixComponent;