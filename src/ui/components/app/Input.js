import React from 'react';

const ContentInput = ({ inputType, placeholder }) => {
  // Validar el tipo de input y establecer el atributo correspondiente
  const inputProps = {};
  switch (inputType) {
    case 'text':
      inputProps.type = 'text';
      break;
    case 'email':
      inputProps.type = 'email';
      break;
    case 'number':
      inputProps.type = 'number';
      inputProps.pattern = '[0-9]*'; // Patrón para aceptar solo números enteros
      break;
    case 'decimal':
      inputProps.type = 'text'; // Puedes usar 'number' también para aceptar solo números
      inputProps.pattern = '^[0-9]*[.,]?[0-9]+$'; // Patrón para aceptar números decimales
      break;
    default:
      inputProps.type = 'text';
  }

  return (
    <div style={{ display: 'flex', alignItems: 'center', padding: '5px' }}>
      <input {...inputProps} placeholder={placeholder} style={{ marginRight: '10px', border: 'none', outline: 'none', padding: '5px',background:'#009940'}} />
    </div>
  );
};

export default ContentInput;
