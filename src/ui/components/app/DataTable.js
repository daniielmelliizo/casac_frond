import React from 'react';
import "../../styles/Datatable.css";
import { FaCheck, FaSearch, FaEye } from "react-icons/fa";

const DataTable = ({ headers, visibles, rows, handleEdit, handleDelete, type, handleForm }) => {
  return (
    <div className="table-container">
      <table>
        <thead>
          <tr>
            {headers.map((header, index) => (
              <th key={index}>{header}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {rows.map((row, rowIndex) => (
            <tr key={rowIndex}>
              <td>
                {`${row[1]} ${row[2]}`}
              </td>
              {visibles.slice(2).map((visibleKey, cellIndex) => ( 
                <td key={cellIndex}>
                  {row[visibleKey]}
                </td>
              ))}
              <td>
                <div className='btns_table'>
                  <button
                    className="actions"
                    data-tooltip="Editar"
                    onClick={() => handleEdit(row)}
                  >
                    Editar
                  </button>
                  <button
                    className="actions"
                    data-tooltip="Eliminar"
                    onClick={() => handleDelete(row)}
                  >
                    Eliminar
                  </button>
                  <button
                    className="actions"
                    data-tooltip="Formularios"
                    onClick={() => handleForm(row)}
                  >
                    <FaEye />
                  </button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default DataTable;
