import React from 'react';
import * as XLSX from 'xlsx';
import '../../styles/Export_Excel_style.css';

const ExportToExcel = ({ chartData, name, type }) => {
  const determineFileName = () => {
    return type === 'finca' ? `Finca_${name}.xlsx` :
           type === 'region' ? `Region_${name}.xlsx` : 
           type === 'Respuestas' ? `Respuesta_${name}.xlsx` : 'reporteInstance.xlsx';
           
  };

  const exportToExcel = (event) => {
    event.preventDefault();

    if (!chartData || chartData.length === 0) {
      console.error('No data to export:', chartData);
      alert('No data available for export.');
      return;
    }

    const wb = XLSX.utils.book_new();
    const ws = XLSX.utils.json_to_sheet(chartData);

    // Apply styles here if needed, first check if the base export works without styling
    XLSX.utils.book_append_sheet(wb, ws, "Data");
    const wbout = XLSX.write(wb, { type: 'array', bookType: 'xlsx' });
    const fileName = determineFileName();
    const blob = new Blob([wbout], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });

    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    document.body.appendChild(a);
    a.href = url;
    a.download = fileName;
    a.click();
    URL.revokeObjectURL(url);
    document.body.removeChild(a);
  };

  return (
    <button onClick={exportToExcel} className="excel-export-button">Exportar a Excel</button>
  );
};

export default ExportToExcel;
