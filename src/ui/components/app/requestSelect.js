import React from 'react';

const SearchInput = ({ type,placeholder, respuesta, handleRespuesta}) => {
  return (
      <div >
        <input
          type={type}
          placeholder={placeholder}
          value={respuesta}
          onChange={handleRespuesta}
        />
      </div>
  );
};

export default SearchInput;
