import React, { useState } from 'react';

const RadioGroup = ({ questionIndex, campo, onRadioChange }) => {
  const Radio = ({ label, value }) => {
    const [isChecked, setChecked] = useState(false);

    const handleRadioChange = () => {
      setChecked(true);

      // Llamar a la función de devolución de llamada si se proporciona
      if (onRadioChange) {
        onRadioChange(questionIndex, label, value);
      }
    };

    return (
      <div key={label}>
        <input
          className="input"
          type="radio"
          checked={isChecked}
          onChange={handleRadioChange}
          value={value}
          name={campo.etiqueta}
        />
        <span>{label}</span>
      </div>
    );
  };

  return (
    <div key={questionIndex} className="form-question">
      {campo.opciones &&
        campo.opciones.map((opcion, index) => (
          <Radio
            key={index}
            label={opcion.etiqueta}
            value={campo.valor}
          />
        ))}
    </div>
  );
};

export default RadioGroup;
