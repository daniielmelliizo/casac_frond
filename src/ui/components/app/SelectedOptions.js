import React from 'react';

const SelectOptions = ({ id, value, onChange, options }) => {
  return (
    <div className="order-by">
      <p>Ordenar por</p>
      <select id={id} value={value} onChange={onChange}>
        {options.map((option) => (
          <option key={option.value} value={option.value}>
            {option.label}
          </option>
        ))}
      </select>
    </div>
  );
};

export default SelectOptions;
