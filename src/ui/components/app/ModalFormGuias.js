import React from 'react';
import Modal from 'react-modal';

const ModalFormGuias = ({
  modalOpenCampos,
  handleModalClose,
  tipoCampo,
  setTipoCampo,
  campoOptions,
  setCampoOptions,
  handleClick,
  arrayCampoOptions,
  setArrayCampoOptions,
  filasOption,
  setFilasOption,
  handleClickFilas,
  arrayFilasOption,
  setArrayFilasOption,
  columnasOption,
  setColumnasOption,
  handleClickColumnas,
  arrayColumnasOption,
  setArrayColumnasOption,
  renderCampo,
  handleGuardarCampo
}) => {
  return (
    <Modal isOpen={modalOpenCampos} onRequestClose={handleModalClose}>
      <div style={styles.modalContent}>
        <div style={styles.row}>
          <div style={styles.column}>
            <h2>{tipoCampo.tipo}</h2>
            <h2>Ingrese el título de la pregunta</h2>
            <input
              type="text"
              value={tipoCampo.etiqueta}
              onChange={(e) => setTipoCampo({ ...tipoCampo, etiqueta: e.target.value })}
            />
            {tipoCampo.tipo !== "checkbox" &&
              tipoCampo.tipo !== "opcion_unica" &&
              tipoCampo.tipo !== "lista" && (
                <div>
                  <h2>Ingrese la descripción de la pregunta</h2>
                  <input
                    type="text"
                    value={tipoCampo.placeholder}
                    onChange={(e) => setTipoCampo({ ...tipoCampo, placeholder: e.target.value })}
                  />
                </div>
              )}
            <h2>Ingrese el nombre por debajo</h2>
            <input
              type="text"
              value={tipoCampo.nombre}
              onChange={(e) => setTipoCampo({ ...tipoCampo, nombre: e.target.value })}
            />
            <h2>Ingrese si es requerido o no</h2>
            <input
              type="text"
              value={tipoCampo.requerido}
              disabled
              onChange={(e) => setTipoCampo({ ...tipoCampo, requerido: e.target.value })}
            />
            <button
              style={styles.btn_m}
              onClick={() =>
                setTipoCampo({
                  ...tipoCampo,
                  requerido: tipoCampo.requerido === "true" ? "false" : "true",
                })
              }
            >
              {tipoCampo.requerido === "true" ? "Obligatorio" : "No Obligatorio"}
            </button>
          </div>
          <div style={styles.column}>
            {tipoCampo.tipo === "checkbox" ||
              tipoCampo.tipo === "opcion_unica" ||
              tipoCampo.tipo === "lista" ? (
                <div>
                  <h2>Ingrese las opciones</h2>
                  <input
                    value={campoOptions}
                    onChange={(e) => setCampoOptions(e.target.value)}
                  />
                  <button style={styles.btn_m} onClick={handleClick}>Insertar</button>
                  <ul>
                    {arrayCampoOptions.map((campo) => (
                      <li key={campo.valor}>
                        {campo.etiqueta}
                        <button
                          onClick={() => {
                            setArrayCampoOptions(
                              arrayCampoOptions.filter((a) => a.etiqueta !== campo.etiqueta)
                            );
                          }}
                        >
                          Eliminar
                        </button>
                      </li>
                    ))}
                  </ul>
                </div>
              ) : null}

            {tipoCampo.tipo === "matriz" && (
              <div>
                <div>
                  <h2>Ingrese las fila</h2>
                  <input
                    value={filasOption}
                    onChange={(e) => setFilasOption(e.target.value)}
                  />
                  <button style={styles.btn_m} onClick={handleClickFilas}>Insertar</button>
                  <ul>
                    {arrayFilasOption.map((filas) => (
                      <li key={filas.valor}>
                        {filas.etiqueta}
                        <button
                          onClick={() => {
                            setArrayFilasOption(
                              arrayFilasOption.filter((a) => a.etiqueta !== filas.etiqueta)
                            );
                          }}
                        >
                          Eliminar
                        </button>
                      </li>
                    ))}
                  </ul>
                </div>
                <div>
                  <h2>Ingrese las columnas</h2>
                  <input
                    value={columnasOption}
                    onChange={(e) => setColumnasOption(e.target.value)}
                  />
                  <button style={styles.btn_m} onClick={handleClickColumnas}>Insertar</button>
                  <ul>
                    {arrayColumnasOption.map((columnas) => (
                      <li key={columnas.valor}>
                        {columnas.etiqueta}
                        <button
                          onClick={() => {
                            setArrayColumnasOption(
                              arrayColumnasOption.filter((a) => a.etiqueta !== columnas.etiqueta)
                            );
                          }}
                        >
                          Eliminar
                        </button>
                      </li>
                    ))}
                  </ul>
                </div>
              </div>
            )}
          </div>
        </div>
        <div style={styles.row}>
          <div style={styles.column}>
            {tipoCampo.etiqueta}
            {renderCampo(tipoCampo, 0)}
          </div>
          <div style={styles.column}>
            <button className='btn_m' onClick={handleGuardarCampo}>Guardar Campo</button>
            <button className='btn_m' onClick={handleModalClose}>Cancelar</button>
          </div>
        </div>
      </div>
    </Modal>
  );
};

const styles = {
  modalContent: {
    display: 'flex',
    width: '25%',
    
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: '20px',
  },
  column: {
    flex: 1,
    marginRight: '10px',
  },
  btn_m: {
    backgroundColor: '#009940',
    color: 'white',
    padding: '10px',
    border: 'none',
    borderRadius: '5px',
    cursor: 'pointer',
    marginRight: '10px',
  },
};

export default ModalFormGuias;
