import React from 'react';

const CustomTextarea = ({ questionIndex, campo }) => {
  // Propiedades del textarea
  const textareaProps = {
    key: questionIndex,
    placeholder: campo.placeholder,
    required: campo.requerido,
    className: "custom-textarea",  // Ajusta según tus necesidades
  };

  return (
    <div style={{ display: 'flex', flexDirection: 'column'}}>
      <textarea {...textareaProps} style={{ border: 'none', outline: 'none', padding: '10px', marginBottom: '10px' }} />
    </div>
  );
};

export default CustomTextarea;