import React, { useState, useEffect } from "react";
import "../../styles/Datatable.css";
import UserService from "../../../application/services/usersService";
import UserController from "../../../interfaz/controllers/userController";
import ExportToExcel from "./Export_Excel";
import SearchInput from "./SearchInput";
import { ColorRing } from "react-loader-spinner";
import "react-datepicker/dist/react-datepicker.css";

const userService = new UserService();
const userController = new UserController(userService);

const DataTableRespuesta = ({ headers, handleEdit, id, titulo }) => {
  const [form, setForm] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  const [selectedDate, setSelectedDate] = useState("");
  const [loading, setLoading] = useState(true);

  const currentPosts = form.filter((forms) => {
    const fullName = `${forms.formulario[0].nombreFormulario}`.toLowerCase();
    const fecha = `${forms.datosPersona[0].fecha}`.toLowerCase();
    return (
      fullName.includes(searchTerm.toLowerCase()) &&
      fecha.includes(selectedDate.toLowerCase())
    );
  });
  const handleSearch = (e) => {
    setSearchTerm(e.target.value);
  };
  const handleDateChange = (e) => {
    const selectedDate = e.target.value.replace(/-/g, "/");
    setSelectedDate(selectedDate);
    console.log("como esta mandando la fechha", selectedDate);
  };

  const handleGetUserss = async () => {
    try {
      const response = await userController.handleParametricas(id);
      const usersData = response.formularioEncontrado || []; // Extract the array from the object
      setForm(usersData);
      setLoading(false);
    } catch (error) {
      console.error("Error al obtener los usuarios:", error.message);
    }
  };
  useEffect(() => {
    const handleGetUserss = async () => {
      try {
        console.log("que id me trae", id);
        const response = await userController.handleParametricas(id);
        const usersData = response.formularioEncontrado || []; // Extract the array from the object
        setForm(usersData);
      setLoading(false);

      } catch (error) {
        console.error("Error al obtener los forms:", error.message);
      }
    };

    handleGetUserss();
  }, []);
  if (loading) {
    return <div className="loading-modal"><ColorRing
    visible={true}
    height="80"
    width="80"
    ariaLabel="blocks-loading"
    wrapperStyle={{}}
    wrapperClass="blocks-wrapper"
    colors={['#e15b64', '#f47e60', '#f8b26a', '#abbd81', '#849b87']}
  /> <h3>cargando ....</h3> </div>;
  }

  const handleDownloadReport = (sessionDataDetail) => {
    console.log("que me trae", sessionDataDetail);
    let dataForExcel = [];
  
    const sesiones = sessionDataDetail.sesiones;
  
    if (!sesiones || sesiones.length === 0) {
      console.error('No se encontraron sesiones en sessionDataDetail.formularioEncontrado');
      return [];
    }
  
    sesiones.forEach(sesion => {
      sesion.respuesta.forEach(respuesta => {
        if (Array.isArray(respuesta.respuesta)) {
          respuesta.respuesta.forEach((element, index) => {
            if (element && typeof element === 'object') {
              const rowData = {
                'Sesión': sesion.nombre,
                'NombrePregunta': respuesta.nombre,
                'EtiquetaPregunta': element.etiqueta || `Subparte ${index}`,
                'Valor': element.etiqueta
              };
              
              // Si el nombre de la pregunta es 'ubicacion' o 'gpsfinca', agrega el campo Coordenadas.
              if (respuesta.nombre === 'ubicacion' || respuesta.nombre === 'gpsfinca') {
                rowData['Coordenadas'] = `Latitud: ${element.nombre}, Longitud: ${element.valor}`;
              }
              
              dataForExcel.push(rowData);
            }
          });
        } else if (respuesta.respuesta && typeof respuesta.respuesta === 'object' && respuesta.respuesta.valor !== undefined) {
          const rowData = {
            'Sesión': sesion.nombre,
            'NombrePregunta': respuesta.nombre,
            'EtiquetaPregunta': "",
            'Valor': respuesta.respuesta.valor
          };
          
          // Si el nombre de la pregunta es 'ubicacion' o 'gpsfinca', agrega el campo Coordenadas.
          if (respuesta.nombre === 'ubicacion' || respuesta.nombre === 'gpsfinca') {
            rowData['Coordenadas'] = `Latitud: ${respuesta.respuesta.nomre}, Longitud: ${respuesta.respuesta.valor}`;
          }
  
          dataForExcel.push(rowData);
        }
      });
    });
  
    return dataForExcel;
  };
  
  return (
    <div className="content_u">
      <h2>{titulo}</h2>
      <div className="bar_search_u">
        <SearchInput searchTerm={searchTerm} handleSearch={handleSearch} />
        <input
          type="date"
          selected={selectedDate}
          onChange={handleDateChange}
        />
      </div>
      {currentPosts.length === 0 ? (
        <h2>no hay respuestas de este usuario</h2>
      ): (
        <table>
          <thead>
            <tr>
              {headers.map((header, index) => (
                <th key={index}>{header}</th>
              ))}
            </tr>
          </thead>
          <tbody>
            {currentPosts.map((row, rowIndex) => (
              <tr key={rowIndex}>
                <td>{`${row.formulario[0].nombreFormulario}`}</td>
                <td>{`${row.datosPersona[0].fecha}`}</td>
                <td>
                  <div className="btns_table">
                    <ExportToExcel chartData={handleDownloadReport(row)} name={row.formulario[0].nombreFormulario} type={"Respuestas"} />
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      )}
    </div>
  );
};

export default DataTableRespuesta;
