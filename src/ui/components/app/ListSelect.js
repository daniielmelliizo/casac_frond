import React from 'react';

const ListaSelect = ({ questionIndex, campo }) => {
  return (
    <select key={questionIndex} className="selectform">
      {campo.opciones && campo.opciones.map((opcion, index) => (
        <option key={index} value={opcion.valor}>
          {opcion.etiqueta}
        </option>
      ))}
    </select>
  );
};

export default ListaSelect;
