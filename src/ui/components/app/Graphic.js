import React, { useEffect } from 'react';
import Chart from 'chart.js/auto';
import PropTypes from 'prop-types'; // Importar PropTypes para validar las propiedades

const RadarChart = ({ chartData, chartOptions, additionalData }) => {
  useEffect(() => {
    // Función para crear o actualizar el gráfico de radar
    const createOrUpdateRadarChart = (canvasId, data, options) => {
      const canvas = document.getElementById(canvasId);
      if (!canvas) {
        console.error(`No se encontró el elemento canvas con el ID ${canvasId}`);
        return;
      }

      const ctx = canvas.getContext('2d');
      if (!ctx) {
        console.error(`No se pudo obtener el contexto 2D del canvas con el ID ${canvasId}`);
        return;
      }

      // Intentar obtener el gráfico existente
      const existingChart = Chart.getChart(ctx);

      // Destruir el gráfico existente si hay uno
      if (existingChart) {
        existingChart.destroy();
      }

      // Crear el gráfico de radar
      new Chart(ctx, {
        type: 'radar',
        data: data,
        options: {
          ...options,
          plugins: {
            labels: {
              render: 'label',
              fontColor: '#000000',
              fontSize: 12,
              position: 'outside',
            },
          },
        },
      });
    };

    // Crear o actualizar gráfico
    createOrUpdateRadarChart('pentagon-chart', chartData, chartOptions);
  }, [chartData, chartOptions]); // Dependencias actualizadas

  return (
    <div style={{ width: '30%', margin: '7px', padding: '5px', border: '1px solid #000000', borderRadius: '8px', boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)' }}>
      <h3>{additionalData.title}</h3>
      <canvas id="pentagon-chart" width="300" height="300"></canvas>
      <div className="contenet_dats">
        {Object.entries(additionalData).map(([key, value]) => (
          <p key={key}><span>{value.icon}</span>{value.text}</p>
        ))}
      </div>
    </div>
  );
};

RadarChart.propTypes = {
  chartData: PropTypes.object.isRequired,
  chartOptions: PropTypes.object.isRequired,
  additionalData: PropTypes.object.isRequired,
};

export default RadarChart;
