import React from 'react';

const IconButton = ({ text, icon, onClick, className }) => {
  return (
    <button className={className} onClick={onClick}>
      {icon}
      <span>{text}</span>
    </button>
  );
};

export default IconButton;
