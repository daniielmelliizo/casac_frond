import React from 'react';
import { FaImage, FaVolumeUp, FaSignature, FaPeriscope } from 'react-icons/fa';

const ContentIcon = ({ contentType, placeholder }) => {
  // Mapa de iconos para cada tipo de contenido
  const contentIcons = {
    foto: <FaImage />,
    audio: <FaVolumeUp />,
    firma: <FaSignature />,
    gps: <FaPeriscope />,
    // Puedes agregar más tipos según tus necesidades
  };

  // Obtener el icono correspondiente según el tipo de contenido
  const contentIcon = contentIcons[contentType] || null;

  return (
    <div style={{ display: 'flex', alignItems: 'center', borderRadius: '10px', padding: '5px', border: '1px solid #ccc' }}>
      <div style={{ marginRight: '10px' }}>{contentIcon}</div>
      <div>{placeholder}</div>
    </div>
  );
};

export default ContentIcon;
