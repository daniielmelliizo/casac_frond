import React, { useState } from 'react';

const DateTimePicker = ({ onSelectDateTime }) => {
  const [selectedDateTime, setSelectedDateTime] = useState('');

  const handleDateTimeChange = (event) => {
    const dateTimeValue = event.target.value;
    setSelectedDateTime(dateTimeValue);

    // Puedes convertir el valor a un objeto de fecha si lo necesitas
    const parsedDateTime = new Date(dateTimeValue);
    onSelectDateTime(parsedDateTime);
  };

  return (
    <div>
      <label>
        Seleccionar Fecha y Hora:
        <input type="datetime-local" value={selectedDateTime} onChange={handleDateTimeChange} />
      </label>
    </div>
  );
};

export default DateTimePicker;
