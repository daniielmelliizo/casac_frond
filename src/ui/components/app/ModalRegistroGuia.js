import React from 'react';
import Modal from 'react-modal';
import '../../styles/ModalRegistroGuia.css'; // Importar los estilos CSS

const ModalRegistroGuia = ({ isOpen, onRequestClose, nombreSesion, descripcionSesion, setNombreSesion, setDescripcionSesion, handleGuardarSesion, handleModalClose }) => {
  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      contentLabel="Agregar Guía"
      className="Modal_sec" 
      
    >
      <div>
        <h1>Nueva Seccion</h1>
        <h2>Ingrese el nombre de la nueva sesión</h2>
        <input
          type="text"
          value={nombreSesion}
          onChange={(e) => setNombreSesion(e.target.value)}
        />
        <h2>Ingrese la descripción de la nueva sesión</h2>
        <input
          type="text"
          value={descripcionSesion}
          onChange={(e) => setDescripcionSesion(e.target.value)}
        />
        <div className="btns_sec">
          <button className="save_btn" onClick={handleGuardarSesion}>Guardar</button>
          <button className="cancel_btn" onClick={handleModalClose}>Cancelar</button>
        </div>
      </div>
    </Modal>
  );
};

export default ModalRegistroGuia;
