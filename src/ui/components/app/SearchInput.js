import React from 'react';
import { FaSearch } from 'react-icons/fa';

const SearchInput = ({ searchTerm, handleSearch}) => {
  return (
      <div className="search-bar">
        <FaSearch />
        <input
          type="text"
          placeholder="Buscar..."
          value={searchTerm}
          onChange={handleSearch}
        />
      </div>
  );
};

export default SearchInput;
