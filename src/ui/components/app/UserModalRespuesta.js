// UserModal.js

import React from "react";
import Modal from "react-modal";
import "../../styles/Usermodalstyle.css";
import DataTableRespuesta from "../app/DataTableRespuesta";

const UserModalRespuesta = ({
  isOpen,
  onRequestClose,
  contentLabel,
  handleCloseModal,
  idUsers,
  titulo,
}) => {
  const visibles = [1, 2, 3, 10, 9];
  const headers = ["formulario", "fecha registrada", "Opciones"];

  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      contentLabel={contentLabel}
      className="custom-modal"
    >
      <div className="modal-content-wrapper">
        <DataTableRespuesta
          headers={Array.isArray(headers) ? headers : []}
          visibles={visibles}
          type={"users"}
          id={idUsers}
          titulo={titulo}
        />
        <button
          className="close-button"
          data-tooltip="Cerrar"
          onClick={handleCloseModal}
        >
          Cerrar
        </button>
      </div>
    </Modal>
  );
};

export default UserModalRespuesta;
