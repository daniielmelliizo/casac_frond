import React, { useState } from 'react';
import { FaClipboard, FaArchive, FaChartBar, FaUsers, FaHome} from 'react-icons/fa';
import { Link } from 'react-router-dom';
import '../../styles/Navbar.css';

const Navbar = () => {
  const [isHovered, setIsHovered] = useState(false);

  

  return (
    <div
      className={`vertical-navbar ${isHovered ? 'hovered' : ''}`}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      <nav>
        <ul>
          <li>
            <Link to="/home">
              <FaHome />
              {isHovered && <span>Inicio</span>}
            </Link>
          </li>
          <li>
            <Link to="/guias">
              <FaClipboard />
              {isHovered && <span>Creacion Guias</span>}
            </Link>
          </li>
          <li>
            <Link to="/despliegues">
              <FaArchive />
              {isHovered && <span>Formulario</span>}
            </Link>
          </li>
          <li>
            <Link to="/reportes" >
              <FaChartBar />
              {isHovered && <span>Reportes</span>}
            </Link>
          </li>
          <li>
            <Link to="/gestion-usuarios">
              <FaUsers />
              {isHovered && <span>Usuarios</span>}
            </Link>
          </li>
        </ul>

      </nav>
    </div>
  );
};

export default Navbar;
