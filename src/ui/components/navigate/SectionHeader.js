// header.js
import React from 'react';
import { FaHome, FaUserCircle,FaSignOutAlt} from 'react-icons/fa';
import { Link } from 'react-router-dom';
import '../../styles/SectionHeader.css';
import logoSrc from '../../images/Logo.png';
import Tittle from '../../images/Tittle_Logo.png';
import Rectangulo from '../../components/navigate/Colorbar';

const Header = ({onLogout }) => {

    const handleLogout = () => {
        onLogout();
      };

      const nameUser = localStorage.getItem("name");

  return (
    <div className="complet">
    <div className="header">
      <div className="logo">
        <img src={logoSrc} alt="Logo" />
      </div>
      <div className='separador'>
       <div className="title">
         <img className="Casac" src={Tittle} alt="Tittle" />
         <p>Caficultura Agroecológica Sustentable
           para la Adaptación al Cambio Climático</p>
       </div>
       <div className="divider"></div>
      </div>
      <div className="user-info">
        <Link to="/home">
        <div className='lin'>
          <FaHome />
          <p>Inicio</p>
        </div>
        </Link>
      </div>
      <div className="user">
            <Link to="/Profile">
            <div className='lin'>
                <FaUserCircle />
                <span className='btn_us'>{nameUser}</span>
                </div>
              </Link>

          <Link onClick={handleLogout}>
                <FaSignOutAlt />
            </Link>
        </div>
    </div>
    <div className="rectangulos">
    {['#012B31', '#E95837', '#B3865E', '#F08235','#9CC430','#009940','#4886C6','#1B6E81','#012B31'].map((color, index) => (
      <Rectangulo key={index} color={color} />
    ))}
  
    </div>
    </div>
  );
}

export default Header;
