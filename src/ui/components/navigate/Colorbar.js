// Rectangulo.js
import React from 'react';
import '../../styles/Colorbar.css';

const Rectangulo = ({ color }) => {
  return <div className="rectangulo" style={{ backgroundColor: color }}></div>;
}

export default Rectangulo;
