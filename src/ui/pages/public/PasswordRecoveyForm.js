import React, { useState } from 'react';
import "../../styles/LoginForm.css";

const PasswordRecoveryForm = ({ onCancel, onRecover }) => {
  const [email, setEmail] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    // Lógica para enviar la solicitud de recuperación de contraseña
    onRecover(email);
  };

  return (
    <div className='login-container'>
      <h2> Contraseña</h2>
      <form onSubmit={handleSubmit}>
        <label>
          Email:
          <input type="email" value={email} onChange={(e) => setEmail(e.target.value)} />
        </label>
        <br />
        <button className="login" type="submit">Recuperar Contraseña</button>
        <button className="login" type="button" onClick={onCancel}>Cancelar</button>
      </form>
    </div>
  );
};

export default PasswordRecoveryForm;
