import React, { useState } from "react";
import PasswordRecoveryForm from "./PasswordRecoveyForm";
import "../../styles/LoginForm.css";
import { FaEnvelope, FaEye, FaEyeSlash } from "react-icons/fa";
import LoginImagen from "../../images/coffe.jpg";
import Tittle from '../../images/Tittle_Logo.png';
import Logo from '../../images/Logo.png';
import Rectangulo from "../../components/navigate/Colorbar";

const LoginForm = ({ onLogin }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [showRecoveryForm, setShowRecoveryForm] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault(); // Evitar la recarga de la página por defecto
    onLogin(email, password, e); // Pasa la información del usuario al callback proporcionado en las props
  };

  // const handleRecoveryClick = () => {
  //   setShowRecoveryForm(true);
  // };

  const handleCancelRecovery = () => {
    setShowRecoveryForm(false);
  };

  const handleRecoverPassword = (recoveryEmail) => {
    // Lógica para manejar la recuperación de contraseña
    console.log(
      `Solicitud de recuperación de contraseña para: ${recoveryEmail}`
    );
    // Aquí podrías realizar acciones adicionales, como enviar un correo electrónico de recuperación
    setShowRecoveryForm(false);
  };

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  return (
    <div className="LoginContainerPrincipal">
      <div className="image-container">
        <img src={LoginImagen} alt="Login" />
      </div>
      <div className="login-container">
        <form onSubmit={handleSubmit}>
        <h2><img className="Logo" src={Logo} alt="Tittle" /></h2>
          <h1><img className="Titulo" src={Tittle} alt="Tittle" /></h1>
          <div className="rectangulos">
            {['#012B31', '#E95837', '#B3865E', '#F08235','#9CC430','#009940','#4886C6','#1B6E81','#012B31'].map((color, index) => (
            <Rectangulo key={index} color={color} />
            ))}
  
          </div>
          <h2 className="subtitutlo">Inicio de Sesión</h2>
          <div className="input-container">
            <input
            className="correo"
              type="email"
              placeholder="Correo electrónico"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <FaEnvelope className="input-icon" />
          </div>
          <div className="input-container">
            <input
              className="password"
              type={showPassword ? "text" : "password"}
              placeholder="Contraseña"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            {showPassword ? (
              <FaEyeSlash
                className="input-icon"
                onClick={togglePasswordVisibility}
              />
            ) : (
              <FaEye
                className="input-icon"
                onClick={togglePasswordVisibility}
              />
            )}
          </div>
          {/* <label htmlFor="remember">Recordar contraseña</label>
          <a href="remeberold" onClick={handleRecoveryClick}>
            Olvido de contraseña
          </a>  */}
          
          <button className="login_btn" type="submit">
            Login
          </button>
        </form>
        {showRecoveryForm && (
          <PasswordRecoveryForm
            onCancel={handleCancelRecovery}
            onRecover={handleRecoverPassword}
          />
        )}
      </div>
      
    </div>
  );
};

export default LoginForm;
