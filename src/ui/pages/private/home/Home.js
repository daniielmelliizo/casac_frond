import React from 'react';
import ICONO_CASAC from '../../../images/CASAC-Logo-Final.png';
import "../../../styles/Inicio.css";
const Home = () => {
  
  

  return (
    <div className="Page">
      <div className="Bienvenida">
          <img className="icono-casac" src={ICONO_CASAC} alt="icono-casac" />
      </div>
    </div>
  );
};

export default Home;
