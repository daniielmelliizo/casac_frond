import React, { useState, useEffect } from 'react';
import { FaUserCircle } from 'react-icons/fa';
import "../../../styles/Profile.css";
import UserController from "../../../../interfaz/controllers/userController";
import UserService from "../../../../application/services/usersService";
import { ColorRing } from 'react-loader-spinner';
import { show_alerta } from '../../../../function';


const userService = new UserService();
const userController = new UserController(userService);

const Profile = ({ contentLabel }) => {
  const [userData, setUserData] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [isEditMode, setIsEditMode] = useState(false);
  const [formData, setFormData] = useState(null);
  const [showAlert, setShowAlert] = useState(false);

  useEffect(() => {
    const handleGetUser = async () => {
      try {
        const response = await userController.handleGetUsers();
        const usersData = response.usuarios || [];

        const userId = localStorage.getItem("id");
        const currentUser = usersData.find(user => user.id === userId);

        if (currentUser) {
          setUserData(currentUser);
          setFormData({ ...currentUser }); // Guarda los datos en formData al cargar
        } else {
          setError("No se encontró información del usuario");
        }

        setLoading(false);
      } catch (error) {
        setError(error.message);
        setLoading(false);
        console.error("Error al obtener el usuario:", error.message);
      }
    };

    handleGetUser();
  }, []);

  const handleEdit = () => {
    setIsEditMode(true);
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSaveEdit = async () => {
    try {
      await userController.handleUpdateUsers(userData.id, formData);
      setIsEditMode(false);
      setUserData(formData);
      setShowAlert(true); // Mostrar la alerta después de guardar los cambios
      setTimeout(() => setShowAlert(false), 3000); // Ocultar la alerta después de 3 segundos
      show_alerta("usuario Actualizado exitosamente. ", "success");
    } catch (error) {
      console.error("Error al editar el usuario:", error.message);
    }
  };


  return (
    <div className="Page">
      {loading ? (
        <div className="loading-container">
          <ColorRing
            visible={true}
            height="80"
            width="80"
            ariaLabel="blocks-loading"
            wrapperStyle={{ display: 'flex', margin: '0 auto' }}
            wrapperClass="blocks-wrapper"
            colors={['#e15b64', '#f47e60', '#f8b26a', '#abbd81', '#849b87']}
          />
          <h3>cargando ....</h3>
        </div>
      ) : error ? (
        <p>Error: {error}</p>
      ) : (
        <div className="User-page" key={userData.id}>
          <div className="Content-user-icon">
            <FaUserCircle />
          </div>
          <div className="Content-user">
            <h2>{contentLabel}</h2>
            <form>
              <div className="form-cont">
                <label htmlFor="nombres">Nombre</label>
                <input
                  type="text"
                  id="nombres"
                  name="nombres"
                  value={formData ? formData.nombres : ""}
                  onChange={handleInputChange}
                  required
                  disabled={!isEditMode}
                />
              </div>

              <div className="form-cont">
                <label htmlFor="apellidos">Apellido</label>
                <input
                  type="text"
                  id="apellidos"
                  name="apellidos"
                  value={formData ? formData.apellidos : ""}
                  onChange={handleInputChange}
                  required
                  disabled={!isEditMode}
                />
              </div>

              <div className="form-cont">
                <label htmlFor="identificacion">Identificación</label>
                <input
                  type="number"
                  id="identificacion"
                  name="identificacion"
                  value={formData ? formData.identificacion : ""}
                  onChange={handleInputChange}
                  maxLength="10"
                  minLength="6"
                  required
                  disabled={!isEditMode}
                />
              </div>

              <div className="form-cont">
                <label htmlFor="universidad">Universidad</label>
                <input
                  type="text"
                  id="universidad"
                  name="universidad"
                  value={formData ? formData.universidad : ""}
                  onChange={handleInputChange}
                  required
                  disabled={!isEditMode}
                />
              </div>

              <div className="form-cont">
                <label htmlFor="profesion">Profesión</label>
                <input
                  type="text"
                  id="profesion"
                  name="profesion"
                  value={formData ? formData.profesion : ""}
                  onChange={handleInputChange}
                  required
                  disabled={!isEditMode}
                />
              </div>

              <div className="form-cont">
                <label htmlFor="grupo_investigacion">Grupo de Investigación</label>
                <input
                  type="text"
                  id="grupo_investigacion"
                  name="grupo_investigacion"
                  value={formData ? formData.grupo_investigacion : ""}
                  onChange={handleInputChange}
                  required
                  disabled={!isEditMode}
                />
              </div>

              <div className="form-cont">
                <label htmlFor="rol">Rol</label>
                <select
                  id="rol"
                  name="rol"
                  value={formData ? formData.rol : ""}
                  onChange={handleInputChange}
                  disabled={!isEditMode}
                >
                  <option value="">Selecciona...</option>
                  <option value="investigador">Investigador</option>
                  <option value="promotor">Promotor</option>
                  <option value="productor">Productor</option>
                </select>
              </div>

              <div className="form-cont">
                <label htmlFor="correo">Correo</label>
                <input
                  type="email"
                  id="correo"
                  name="correo"
                  value={formData ? formData.correo : ""}
                  onChange={handleInputChange}
                  required
                  disabled={!isEditMode}
                />
              </div>

              <div className="form-cont">
                <label htmlFor="contrasena">Contraseña</label>
                <input
                  type="password"
                  id="contrasena"
                  name="contrasena"
                  value={formData ? formData.contrasena : ""}
                  onChange={handleInputChange}
                  required
                  disabled={!isEditMode}
                />
              </div>

              <div className="form-cont">
                <label htmlFor="status">Status</label>
                <input
                  type="text"
                  id="status"
                  name="status"
                  value={formData ? formData.status : ""}
                  disabled={true}
                />
              </div>

              <div className="form-cont">
                <label htmlFor="ultimo_acceso">Último Acceso</label>
                <input
                  type="text"
                  id="ultimo_acceso"
                  name="ultimo_acceso"
                  value={formData ? formData.ultimo_acceso : ""}
                  disabled={true}
                />
              </div>
                
              </form>
            <div className='btn-edit-user'>
              
                <button className='btn-user' onClick={handleSaveEdit}>Guardar Cambios</button>
              
                <button type="button" className='btn-user' onClick={handleEdit}>Modificar Usuario</button>
              
            </div>
            {showAlert && (
              <div className="alert-container">
                <p className="alert-message">¡Los cambios han sido guardados exitosamente!</p>
              </div>
            )}
          </div>
        </div>
      )}
    </div>
  );
};

export default Profile;