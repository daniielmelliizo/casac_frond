import React, { useState, useEffect } from "react";
import Modal from "react-modal";
import axios from "axios";
import {
  Chart as ChartJS,
  RadialLinearScale,
  PointElement,
  LineElement,
  Filler,
  Tooltip,
  Legend,
} from "chart.js";
import ExportToExcel from "..//../../../components/app/Export_Excel";
import "../../../../styles/Region_Reporte.css";
import ProgressBar from "../../../../components/app/ProgressBar";
import TrancicionService from "../../../../../application/services/trancicionService";
import TrancicionController from "../../../../../interfaz/controllers/trancicionController";
import { show_alerta } from "../../../../../function";
import Finca from "./Finca";
Modal.setAppElement("#root");

const trancicionService = new TrancicionService();
const trancicionController = new TrancicionController(trancicionService);

ChartJS.register(
  RadialLinearScale,
  PointElement,
  LineElement,
  Filler,
  Tooltip,
  Legend
);

const Finca_Report = ({ fincaName }) => {
  const [sessionData, setSessionData] = useState([]);
  const [formData, setFormData] = useState([]);
  const [sessionDataDetail, setSessionDataDetail] = useState([]);
  const [reportModalOpen, setIsReportModalOpen] = useState(false);
  const [valores, setValores] = useState([
    "1. SUSTENTABILIDAD SOCIO-CULTURAL (ISSC)",
    "2. SUSTENTABILIDAD AGRÍCOLA (ISAG)",
    "3. SUSTENTABILIDAD PECUARIA (ISP)",
    "4. SUSTENTABILIDAD AMBIENTAL (ISAM)",
    "5. SUSTENTABILIDAD ECONÓMICA (ISE)",
  ]);
  const [prueba, setPrueva] = useState({
    nombre: "",
    fechaCreacion: "",
    sesiones: [],
  });
  const [transitionProposal, setTransitionProposal] = useState([]);
  const [activeComponent, setActiveComponent] = useState("Reportes");

  const handleFinca = () => {
    if (activeComponent !== "Finca") {
      setActiveComponent("Finca");
    } else {
      setActiveComponent(null);
    }
  };
  const handleGetTransicion = async () => {
    try {
      const response = await trancicionController.handleConsultar(fincaName);
      const result = response.RutaTransicion || []; // Extract the array from the object
      const nuevasRespuestas = result.sesiones.map(
        (sesion, index) => sesion.respuesta
      );
      setPrueva(result);
      setTransitionProposal(nuevasRespuestas);
      setFormData(nuevasRespuestas);
    } catch (error) {
      console.error("Error al obtener los usuarios:", error.message);
    }
  };

  useEffect(() => {
    const handleGetTransicion = async () => {
      try {
        const response = await trancicionController.handleConsultar(fincaName);
        const result = response.RutaTransicion || []; // Extract the array from the object
        const nuevasRespuestas = result.sesiones.map(
          (sesion, index) => sesion.respuesta
        );
        setPrueva(result);
        setTransitionProposal(nuevasRespuestas);
        setFormData(nuevasRespuestas);
      } catch (error) {
        console.error("Error al obtener los usuarios:", error.message);
      }
    };

    handleGetTransicion();
  }, []);

  const handleSaveTransition = async () => {
    try {
      const updatedFormState = { ...prueba };
      updatedFormState.nombre = fincaName;
      updatedFormState.fechaCreacion = new Date().toLocaleDateString("es-ES");
      if (transitionProposal.length !== 5) {
        show_alerta("faltan campos por llenar", "error");
        return;
      }
      sessionData.map((item, index) => {
        const respuestaSesion = {
          nombre: valores[index],
          respuesta: transitionProposal[index],
        };
        updatedFormState.sesiones.push(respuestaSesion);
      });
      const respuesta = await trancicionController.handleCreateTrancicion(
        updatedFormState
      );
      if (respuesta) {
        show_alerta("Transicion creada exitosamente. ", "success");
        // Redirige a la página de caracterización con el título de la guía
        setActiveComponent("Finca");
      } else {
        show_alerta(
          "Error al crear la guía. Por favor, inténtalo de nuevo.",
          "error"
        );
      }
    } catch (error) {
      console.error("Error al registrar transicion:", error.message);
      show_alerta(
        "Error al crear la ruta de transicion. Por favor, inténtalo de nuevo.",
        "error"
      );
    }
  };

  const handleEditTransition = async () => {
    try {
      const updatedFormState = { ...prueba };
      updatedFormState.nombre = fincaName;
      updatedFormState.fechaCreacion = new Date().toLocaleDateString("es-ES");
      if (transitionProposal.some(value => !value)) {
        show_alerta("Por favor, complete todos los campos.", "error");
        return;
      }
      const updatedSessions = prueba.sesiones.map((sesion, index) => ({
        ...sesion,
        nombre: valores[index],
        respuesta: transitionProposal[index],
      }));
      // Asignar las sesiones actualizadas al estado
      updatedFormState.sesiones = updatedSessions;
      const respuesta = await trancicionController.handleUpdateTrancicion(
        fincaName,
        updatedFormState
      );
      if (respuesta) {
        show_alerta("Transicion Actualizada exitosamente. ", "success");
        // Redirige a la página de caracterización con el título de la guía
        setActiveComponent("Finca");
      } else {
        show_alerta(
          "Error al Actualizar la Transicición. Por favor, inténtalo de nuevo.",
          "error"
        );
      }
    } catch (error) {
      console.error("Error al actualizar la transicion:", error.message);
      show_alerta(
        "Error al actualizar la ruta de transicion. Por favor, inténtalo de nuevo.",
        "error"
      );
    }
  };

  const handleProposalChange = (event, index) => {
    const updatedProposals = [...transitionProposal];
    updatedProposals[index] = event.target.value;
    setTransitionProposal(updatedProposals);
  };

  const handleCloseModal = () => {
    setIsReportModalOpen(false);
  };

  const handleOpenModal = () => {
    setIsReportModalOpen(true);
  };

  const calcularPorcentaje = (grupo) => {
    if (!grupo || grupo.length === 0) return 0; // Verificar si el grupo es válido

    const totalIndicadores = grupo.length;
    const sumaIndicadores = grupo.reduce(
      (acumulador, elemento) => acumulador + parseFloat(elemento.promedio),
      0
    ); // Asegúrate de convertir a flotante el promedio
    return parseFloat((sumaIndicadores / totalIndicadores).toFixed(0));
  };

  const calcularPorcentajeOut = (grupo) => {
    if (!grupo || grupo.length === 0) return 0; // Verificar si el grupo es válido

    const totalIndicadores = grupo.length;
    const sumaIndicadores = grupo.reduce(
      (acumulador, elemento) => acumulador + parseFloat(elemento.promedio),
      0
    ); // Asegúrate de convertir a flotante el promedio
    const total = (sumaIndicadores / totalIndicadores - 100) * -1;
    return parseFloat(total.toFixed(0));
  };

  const ColorBarra = (grupo) => {
    const porcentaje = calcularPorcentaje(grupo);
    if (porcentaje >= 0 && porcentaje < 30) {
      return "#E95837";
    } else if (porcentaje >= 30 && porcentaje < 70) {
      return "#F08235";
    } else if (porcentaje >= 70 && porcentaje < 100) {
      return "#009940";
    } else {
      return "#012B31";
    }
  };

  const ColorBarraOut = (grupo) => {
    const porcentaje = calcularPorcentajeOut(grupo);
    if (porcentaje >= 70 && porcentaje < 100) {
      return "#E95837";
    } else if (porcentaje >= 30 && porcentaje < 70) {
      return "#F08235";
    } else if (porcentaje >= 0 && porcentaje < 30) {
      return "#009940";
    } else {
      return "#012B31";
    }
  };

  const createOrUpdateRadarChart = (canvasId, data, options) => {
    const canvas = document.getElementById(canvasId);
    if (!canvas) {
      console.error(`No se encontró el elemento canvas con el ID ${canvasId}`);
      return;
    }

    const ctx = canvas.getContext("2d");
    if (!ctx) {
      console.error(
        `No se pudo obtener el contexto 2D del canvas con el ID ${canvasId}`
      );
      return;
    }

    const existingChart = ChartJS.getChart(ctx);

    if (existingChart) {
      existingChart.destroy();
    }

    new ChartJS(ctx, {
      type: "radar",
      data: data,
      options: options,
    });
  };

  const obtenerIniciales = (cadena) => {
    cadena = cadena.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    cadena = cadena.replace(/\b\d+\b|\bDE\b|\bde\b|\bY\b|\by\b|\./g, "");
    let palabras = cadena.split(" ");

    let iniciales = "";
    palabras.forEach((palabra) => {
      iniciales += palabra.charAt(0).toUpperCase();
    });
    return iniciales;
  };

  useEffect(() => {
    const fetchData = async () => {
      if (!fincaName) return;

      try {
        const [response, detailResponse] = await Promise.all([
          axios.get(
            `https://casac-com.onrender.com/response/reporter_name/${fincaName}`
          ),
          axios.get(
            `https://casac-com.onrender.com/response/reporter_name/${fincaName}/detail`
          ),
        ]);

        setSessionDataDetail(detailResponse.data);

        const groupsData = response.data.filter(
          (group) =>
            group.length > 0 && group[0].nombreSesion !== "0.1 Datos Generales"
        );

        setSessionData(groupsData);
      } catch (error) {
        console.error("Error al obtener los datos:", error);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    if (sessionData.length > 0) {
      sessionData.forEach((group, index) => {
        const canvasId = `pentagon-chart-${index + 1}`;
        if (document.getElementById(canvasId)) {
          const formattedData = {
            labels: group.map((item) => obtenerIniciales(item.nombreSesion)),
            datasets: [
              {
                label: `Grupo ${index + 1}`,
                data: group.map((item) => parseFloat(item.promedio)),
                backgroundColor:
                  transparentColors[index % transparentColors.length],
                borderColor: colors[index % colors.length],
                borderWidth: 2.5,
              },
            ],
          };
          createOrUpdateRadarChart(canvasId, formattedData, options);
        } else {
          console.error(`Canvas with ID ${canvasId} not found`);
        }
      });
    }
  }, [sessionData]); // Dependencia en sessionData para asegurar recarga cuando cambie.

  const handleDownloadReport = () => {
    let dataForExcel = [];

    if (
      !sessionDataDetail ||
      !sessionDataDetail.formularioEncontrado ||
      sessionDataDetail.formularioEncontrado.length === 0
    ) {
      console.error("No se encontraron datos en sessionDataDetail");
      return [];
    }

    const sesiones = sessionDataDetail.formularioEncontrado.sesiones;

    if (!sesiones || sesiones.length === 0) {
      console.error(
        "No se encontraron sesiones en sessionDataDetail.formularioEncontrado"
      );
      return [];
    }
    sesiones.forEach((sesion) => {
      // Asumiendo que cada `sesion` tiene una propiedad `respuesta` que es un arreglo de respuestas.
      sesion.respuesta.forEach((respuesta) => {
        if (Array.isArray(respuesta.respuesta)) {
          respuesta.respuesta.forEach((element, index) => {
            if (element && typeof element === "object") {
              dataForExcel.push({
                Sesión: sesion.nombre,
                NombrePregunta: respuesta.nombre,
                EtiquetaPregunta: element.etiqueta || `Subparte ${index}`,
                Valor: element.valor,
              });
            }
          });
        } else if (
          respuesta.respuesta &&
          typeof respuesta.respuesta === "object" &&
          respuesta.respuesta.valor !== undefined
        ) {
          dataForExcel.push({
            Sesión: sesion.nombre,
            NombrePregunta: respuesta.nombre,
            EtiquetaPregunta: "",
            Valor: respuesta.respuesta.valor,
          });
        }
      });
    });

    return dataForExcel;
  };

  const colors = [
    "#012B31",
    "#E95837",
    "#B3865E",
    "#F08235",
    "#9CC430",
    "#009940",
    "#4886C6",
    "#1B6E81",
    "#012B31",
  ];
  const transparentColors = [
    "rgba(1, 43, 49, 0.5)",
    "rgba(233, 88, 55, 0.5)",
    "rgba(179, 134, 94, 0.5)",
    "rgba(240, 130, 53, 0.5)",
    "rgba(156, 196, 48, 0.5)",
    "rgba(0, 153, 64, 0.5)",
    "rgba(72, 134, 198, 0.5)",
    "rgba(27, 110, 129, 0.5)",
    "rgba(1, 43, 49, 0.5)",
  ];

  const options = {
    elements: {
      line: {
        borderWidth: 1,
      },
      point: {
        radius: 6,
      },
    },
    plugins: {
      legend: {
        display: false,
      },
      tooltip: {
        enabled: false,
      },
    },
    scales: {
      r: {
        suggestedMin: 0,
        suggestedMax: 100,
        grid: {
          color: "#000000c2",
        },
        pointLabels: {
          font: {
            size: "15vm",
          },
          color: "#012B31",
        },
        angleLines: {
          color: "#000000c2",
        },
        ticks: {
          color: "#012B31", // Cambiar el color de las marcas de los ejes
          beginAtZero: true,
          min: 0,
          max: 100, // Forzar que el máximo sea 100
        },
      },
    },
  };

  return (
    <div>
      {activeComponent !== "Finca" && (
        <div className="content_region_report">
          <div style={{ display: "flex", flexWrap: "wrap", gap: "20px" }}>
            <div className="Regresar_reports">
              <button onClick={handleOpenModal}>Exportar Reporte</button>{" "}
              {/* Botón para abrir el modal */}
              {formData.length > 0 ? (
                <button onClick={handleEditTransition}>
                  Editar ruta de trancición
                </button>
              ) : (
                <button onClick={handleSaveTransition}>
                  Guardar ruta de transición
                </button>
              )}
              <button onClick={handleFinca} className="btn-volver">
                Volver
              </button>
            </div>
            {sessionData.map((item, index) => (
              <div key={index + 1} className="content_region_report">
                <div className="grafica">
                  <h3>{valores[index]}</h3>
                  <canvas id={`pentagon-chart-${index + 1}`}></canvas>
                  <div>
                    {item.map((element, idx) => (
                      <div key={idx + 1}>
                        <p>
                          <span
                            style={{ fontWeight: "bold", fontSize: "1.2em" }}
                          >
                            {idx + 1}:
                          </span>
                          {element.nombreSesion}:{" "}
                          <span
                            style={{
                              padding: "2px 5px",
                              borderRadius: "4px",
                              color: "white",
                              backgroundColor:
                                element.promedio <= 30
                                  ? "#E95837"
                                  : element.promedio <= 70
                                  ? "#F08235"
                                  : "#009940",
                            }}
                          >
                            {parseFloat(element.promedio).toFixed(2)}
                          </span>
                        </p>
                      </div>
                    ))}
                  </div>
                </div>
                <div className="results">
                  <div className="contentInter">
                    <h3>Resultados de la Guía sesión A Finca: {fincaName}</h3>
                    <p>Resultados: {calcularPorcentaje(sessionData[index])}%</p>
                    <ProgressBar
                      color={ColorBarra(sessionData[index])} // Cambiado a className
                      value={calcularPorcentaje(sessionData[index])}
                      maxValue={100}
                    />
                  </div>
                  <div className="contentInter">
                    <h3>Lo que falta para llegar a la situación ideal</h3>
                    <p>
                      Resultados: {calcularPorcentajeOut(sessionData[index])}%
                    </p>
                    <ProgressBar
                      color={ColorBarraOut(sessionData[index])} // Cambiado a className
                      value={calcularPorcentajeOut(sessionData[index])}
                      maxValue={100}
                    />
                  </div>
                  <div className="contentInter">
                    <h3>Propuesta para ruta de trancición agroecológica: </h3>
                    <p>Nota:</p>
                    <textarea
                      style={{ width: "100%", height: "200px" }}
                      value={
                        transitionProposal[index]
                          ? transitionProposal[index]
                          : ""
                      }
                      onChange={(event) => handleProposalChange(event, index)}
                      placeholder="Ingrese aquí su propuesta..."
                    />
                  </div>
                </div>
              </div>
            ))}
          </div>

          <Modal
            isOpen={reportModalOpen}
            onRequestClose={handleCloseModal}
            contentLabel="Agregar Usuario"
          >
            <h2>Descargar Reporte</h2>
            <form>
              <div>
                <label htmlFor="email">
                  Seleccionar el tipo de exportación
                </label>
                <ExportToExcel
                  chartData={handleDownloadReport()}
                  name={fincaName}
                  type={"finca"}
                />
                <button
                  className="close-button"
                  data-tooltip="Cerrar"
                  onClick={handleCloseModal}
                >
                  Cancelar
                </button>
              </div>
            </form>
          </Modal>
        </div>
      )}
      {activeComponent === "Finca" && <Finca />}
    </div>
  );
};

export default Finca_Report;
