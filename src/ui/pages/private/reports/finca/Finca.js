import React, { useEffect, useState } from 'react';
import { Chart as ChartJS, RadialLinearScale, PointElement, LineElement, Filler, Tooltip, Legend } from 'chart.js/auto';
import "../../../../styles/Region.css";
import {
  BsGlobeAmericas,
  BsBoxes,
} from "react-icons/bs";
import FR from './Finca_Report';

ChartJS.register(RadialLinearScale, PointElement, LineElement, Filler, Tooltip, Legend);

const Finca = () => {
  const [activeComponent, setActiveComponent] = useState(null);
  const [chartData, setChartData] = useState(null);
  const [selectedFinca, setSelectedFinca] = useState(null); // Estado para almacenar el nombre de la finca seleccionada

  const handleReportFincaClick = (fincaName) => {
    if (activeComponent !== "FincaReporte") {
      setActiveComponent("FincaReporte");
      setSelectedFinca(fincaName); // Al hacer clic en una finca, establece el nombre de la finca seleccionada
    } else {
      setActiveComponent(null);
      setSelectedFinca(null); // Al cerrar el componente Finca, restablece el nombre de la finca seleccionada
    }
  };

  const createOrUpdateRadarChart = (canvasId, data, options) => {
    const canvas = document.getElementById(canvasId);
    if (!canvas) {
      console.error(`No se encontró el elemento canvas con el ID ${canvasId}`);
      return;
    }

    const ctx = canvas.getContext('2d');
    if (!ctx) {
      console.error(`No se pudo obtener el contexto 2D del canvas con el ID ${canvasId}`);
      return;
    }

    const existingChart = ChartJS.getChart(ctx);
    if (existingChart) {
      existingChart.destroy();
    }

    new ChartJS(ctx, {
      type: 'radar',
      data: data,
      options: options,
    });
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('https://casac-com.onrender.com/response/reporter_metric3');
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        const data = await response.json();
        setChartData(data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    if (chartData) {
      const colors = ['#012B31', '#E95837', '#B3865E', '#F08235', '#9CC430', '#009940', '#4886C6', '#1B6E81', '#012B31']; // Colores para los gráficos
      const transparentColors = [
        'rgba(1, 43, 49, 0.5)',
        'rgba(233, 88, 55, 0.5)',
        'rgba(179, 134, 94, 0.5)',
        'rgba(240, 130, 53, 0.5)',
        'rgba(156, 196, 48, 0.5)',
        'rgba(0, 153, 64, 0.5)',
        'rgba(72, 134, 198, 0.5)',
        'rgba(27, 110, 129, 0.5)',
        'rgba(1, 43, 49, 0.5)'
      ];
      const options = {
        elements: {
          line: {
            borderWidth: 1, // Ancho de las líneas
          },
          point: {
            radius: 6, // Tamaño de los puntos
          }
        },
        plugins: {
          legend: {
            display: false, // Ocultar la leyenda (si lo deseas)
          },
          tooltip: {
            enabled: false, // Desactivar la información sobre herramientas (si lo deseas)
          }
        },
        scales: {
          r: {
            suggestedMin: 0,
            suggestedMax: 100,
            grid: {
              color: '#000000c2', // Cambiar el color del fondo de las líneas
            },
            pointLabels: {
              font: {
                size: '15vm', // Tamaño de la fuente de los números
              },
              color: '#012B31',
            },
            angleLines: {
              color: '#000000c2', // Cambiar el color de las líneas internas
            },
            ticks: {
              color: '#012B31', // Cambiar el color de las marcas de los ejes
              beginAtZero: true,
              min: 0,
              max: 100, // Forzar que el máximo sea 100
            }
          }
        }
      };

      Object.entries(chartData).forEach(([sector, data], index) => {
        const labels = data.map((item, idx) => idx + 1); // Cambiamos los labels por los números del índice
        const dataForChart = {
          labels: data.map(item => {
            switch (item.grupo) {
              case '1':
                return "ISSC";
              case '2':
                return "ISAG";
              case '3':
                return "ISP";
              case '4':
                return "ISAM";
              case '5':
                return "ISE";
              default:
                return item.grupo; // Si no coincide con ningún caso, devolver el grupo original
            }
          }),
          datasets: [
            {
              label: sector,
              data: data.map(item => parseFloat(item.promedio)),
              backgroundColor: transparentColors[index % transparentColors.length],
              borderColor: colors[index % colors.length],
              borderWidth: 5
            },
          ]
        };

        const canvasId = `pentagon-chart-${index}`;
        createOrUpdateRadarChart(canvasId, dataForChart, options);
      });
    }
  }, [chartData]);

  return (
    <div>
      {activeComponent !== "FincaReporte" && (
        <div className='content_region'>
          <div style={{ display: 'flex', flexWrap: 'wrap', gap: '20px' }}>
            {chartData && Object.entries(chartData).map(([sector, data], index) => (
              <span key={index} onClick={() => handleReportFincaClick(sector)} className="btn-option">
                <div className='container'>
                  <div className='graphics'>
                    <h3>{sector}</h3>
                    <canvas id={`pentagon-chart-${index}`} ></canvas>
                  </div>
                  <div className="border-left"></div>
                  <div className="content_data">
                    <p><BsGlobeAmericas />{sector}</p>
                    <p><BsBoxes /> Promedios de indicadores sociodemograficos</p>
                    <ul>
                      {data.map((item, idx) => (
                        <li key={idx} style={{ color: '#012B31' }}>
                          <span style={{ fontWeight: 'bold', fontSize: '1.2em' }}>{idx + 1}:</span>
                          {(() => {
                            switch (item.grupo) {
                              case '1':
                                return "SUSTENTABILIDAD SOCIO-CULTURAL (ISSC)";
                              case '2':
                                return "SUSTENTABILIDAD AGRÍCOLA (ISAG)";
                              case '3':
                                return "SUSTENTABILIDAD PECUARIA (ISP)";
                              case '4':
                                return "SUSTENTABILIDAD AMBIENTAL (ISAM)";
                              case '5':
                                return "SUSTENTABILIDAD ECONÓMICA (ISE)";
                              default:
                                return item.grupo; // Si no coincide con ningún caso, devolver el grupo original
                            }
                          })()} : {parseFloat(item.promedio).toFixed(2)}
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>
              </span>
            ))}
          </div>
        </div>
      )}
      {activeComponent === "FincaReporte" && <FR fincaName={selectedFinca} />}
    </div>
  );
};

export default Finca;
