// ui/pages/Reportes.js
import React, { useState } from "react";
import "../../../styles/Reportes.css";
import Region from "../../private/reports/region/Region";
import Finca from "../../private/reports/finca/Finca";

const MainNav = ({ onLogout }) => {
  const [activeComponent, setActiveComponent] = useState("Reportes");

  const handleNavClick = (component) => {
    setActiveComponent(component);
  };

  return (
    <div className="content">
    <h1>Página de Reportes</h1>
    <h2 >Analítica</h2>
    <div className="btn">
      <h2>Reporte</h2>
      <div className="btn-options">
        <span onClick={() => handleNavClick("Region")} className="btn-option">
          Region
        </span>
        <span onClick={() => handleNavClick("Finca")} className="btn-option">
          Finca
        </span>
      </div>
    </div>
    <div className="Graphics">
      {activeComponent === "Region" && <Region />}
      {activeComponent === "Finca" && <Finca />}
    </div>
  </div>
);
};

export default MainNav;