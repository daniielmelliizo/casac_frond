import React, { useState, useEffect } from "react";
import Modal from "react-modal";
import axios from "axios";
import {
  Chart as ChartJS,
  RadialLinearScale,
  PointElement,
  LineElement,
  Filler,
  Tooltip,
  Legend,
} from "chart.js";
import ExportToExcel from "..//../../../components/app/Export_Excel";
import "../../../../styles/Region_Reporte.css";
import ProgressBar from "../../../../components/app/ProgressBar";
import TrancicionService from "../../../../../application/services/trancicionService";
import TrancicionController from "../../../../../interfaz/controllers/trancicionController";
import { show_alerta } from "../../../../../function";
import Region from "./Region";

ChartJS.register(
  RadialLinearScale,
  PointElement,
  LineElement,
  Filler,
  Tooltip,
  Legend
);
const trancicionService = new TrancicionService();
const trancicionController = new TrancicionController(trancicionService);

const Region_Reporte = ({ region }) => {
  const [sessionData, setSessionData] = useState([]);
  const [sessionDataDetail, setSessionDataDetail] = useState([]);
  const [reportModalOpen, setIsReportModalOpen] = useState(false);
  const [formData, setFormData] = useState([]);
  const [requestError, setRequestError] = useState(null);
  const [valores, setValores] = useState([
    "1. SUSTENTABILIDAD SOCIO-CULTURAL (ISSC)",
    "2. SUSTENTABILIDAD AGRÍCOLA (ISAG)",
    "3. SUSTENTABILIDAD PECUARIA (ISP)",
    "4. SUSTENTABILIDAD AMBIENTAL (ISAM)",
    "5. SUSTENTABILIDAD ECONÓMICA (ISE)",
  ]);
  const [prueba, setPrueva] = useState({
    nombre: "",
    fechaCreacion: "",
    sesiones: [],
  });
  const [transitionProposal, setTransitionProposal] = useState([]);
  const [activeComponent, setActiveComponent] = useState(null);

  const handleRegion = () => {
    if (activeComponent !== "Region") {
      setActiveComponent("Region");
    } else {
      setActiveComponent(null);
    }
  };

  useEffect(() => {
    const handleGetTransicion = async () => {
      try {
        const response = await trancicionController.handleConsultar(region);
        const result = response.RutaTransicion || []; // Extract the array from the object
        const nuevasRespuestas = result.sesiones.map(
          (sesion, index) => sesion.respuesta
        );
        setPrueva(result);
        setTransitionProposal(nuevasRespuestas);
        setFormData(nuevasRespuestas);
      } catch (error) {
        console.error("Error al obtener los usuarios:", error.message);
      }
    };

    handleGetTransicion();
  }, []);

  const handleSaveTransition = async () => {
    try {
      // Recolecta los datos que deseas guardar
      const updatedFormState = { ...prueba };
      updatedFormState.nombre = region;
      updatedFormState.fechaCreacion = new Date().toLocaleDateString("es-ES");
      if (transitionProposal.length !== 5) {
        show_alerta("faltan campos por llenar", "error");
        return;
      }
      sessionData.map((item, index) => {
        const respuestaSesion = {
          nombre: valores[index],
          respuesta: transitionProposal[index],
        };
        updatedFormState.sesiones.push(respuestaSesion);
      });

      const respuesta = await trancicionController.handleCreateTrancicion(
        updatedFormState
      );

      if (respuesta) {
        show_alerta("Transicion creada exitosamente. ", "success");
        // Redirige a la página de caracterización con el título de la guía
        setPrueva([]);
        setTransitionProposal([]);
        setActiveComponent("Finca");
      } else {
        show_alerta(
          "Error al crear la guía. Por favor, inténtalo de nuevo.",
          "error"
        );
      }
    } catch (error) {}
  };
  const handleEditTransition = async () => {
    try {
      const updatedFormState = { ...prueba };
      updatedFormState.nombre = region;
      updatedFormState.fechaCreacion = new Date().toLocaleDateString("es-ES");
      if (transitionProposal.some(value => !value)) {
        show_alerta("Por favor, complete todos los campos.", "error");
        return;
      }
      const updatedSessions = prueba.sesiones.map((sesion, index) => ({
        ...sesion,
        nombre: valores[index],
        respuesta: transitionProposal[index],
      }));
      updatedFormState.sesiones = updatedSessions;
      const respuesta = await trancicionController.handleUpdateTrancicion(
        region,
        updatedFormState
      );
      if (respuesta) {
        show_alerta("Transicion Actualizada exitosamente. ", "success");
        // Redirige a la página de caracterización con el título de la guía
        setPrueva([]);
        setTransitionProposal([]);
        setActiveComponent("Finca");
      } else {
        show_alerta(
          "Error al Actualizar la Transicición. Por favor, inténtalo de nuevo.",
          "error"
        );
      }
    } catch (error) {
      console.error("Error al actualizar la transicion:", error.message);
      show_alerta(
        "Error al actualizar la ruta de transicion. Por favor, inténtalo de nuevo.",
        "error"
      );
    }
  };
  const handleProposalChange = (event, index) => {
    const updatedProposals = [...transitionProposal];
    updatedProposals[index] = event.target.value;
    setTransitionProposal(updatedProposals);
  };

  const handleCloseModal = () => {
    setIsReportModalOpen(false);
  };

  const calcularPorcentaje = (grupo) => {
    if (!grupo || grupo.length === 0) return 0; // Verificar si el grupo es válido

    const totalIndicadores = grupo.length;
    const sumaIndicadores = grupo.reduce(
      (acumulador, elemento) => acumulador + parseFloat(elemento.promedio),
      0
    ); // Asegúrate de convertir a flotante el promedio
    return parseFloat((sumaIndicadores / totalIndicadores).toFixed(0));
  };

  const calcularPorcentajeOut = (grupo) => {
    if (!grupo || grupo.length === 0) return 0; // Verificar si el grupo es válido

    const totalIndicadores = grupo.length;
    const sumaIndicadores = grupo.reduce(
      (acumulador, elemento) => acumulador + parseFloat(elemento.promedio),
      0
    ); // Asegúrate de convertir a flotante el promedio
    const total = (sumaIndicadores / totalIndicadores - 100) * -1;
    return parseFloat(total.toFixed(0));
  };

  const ColorBarra = (grupo) => {
    const porcentaje = calcularPorcentaje(grupo);
    if (porcentaje >= 0 && porcentaje < 30) {
      return "#E95837";
    } else if (porcentaje >= 30 && porcentaje < 70) {
      return "#F08235";
    } else if (porcentaje >= 70 && porcentaje < 100) {
      return "#009940";
    } else {
      return "#012B31";
    }
  };

  const ColorBarraOut = (grupo) => {
    const porcentaje = calcularPorcentajeOut(grupo);
    if (porcentaje >= 70 && porcentaje < 100) {
      return "#E95837";
    } else if (porcentaje >= 30 && porcentaje < 70) {
      return "#F08235";
    } else if (porcentaje >= 0 && porcentaje < 30) {
      return "#009940";
    } else {
      return "#012B31";
    }
  };

  const createOrUpdateRadarChart = (canvasId, data, options) => {
    const canvas = document.getElementById(canvasId);
    if (!canvas) {
      console.info(`No se encontró el elemento canvas con el ID ${canvasId}`);
      return;
    }

    const ctx = canvas.getContext("2d");
    if (!ctx) {
      console.info(
        `No se pudo obtener el contexto 2D del canvas con el ID ${canvasId}`
      );
      return;
    }

    const existingChart = ChartJS.getChart(ctx);

    if (existingChart) {
      existingChart.destroy();
    }

    new ChartJS(ctx, {
      type: "radar",
      data: data,
      options: options,
    });
  };
  const obtenerIniciales = (cadena) => {
    cadena = cadena.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    cadena = cadena.replace(/\b\d+\b|\bDE\b|\bde\b|\bY\b|\by\b|\./g, "");
    let palabras = cadena.split(" ");

    let iniciales = "";
    palabras.forEach((palabra) => {
      iniciales += palabra.charAt(0).toUpperCase();
    });
    return iniciales;
  };

  useEffect(() => {
    if (!region) return;

    axios
      .get(`https://casac-com.onrender.com/response/reporter_metric2/${region}`)
      .then((response) => {
        const groupsData = response.data.filter(
          (group) =>
            group.length > 0 && group[0].nombreSesion !== "0.1 Datos Generales"
        );

        setSessionData(groupsData);
        setRequestError(null);
      })
      .catch((error) => {
        console.error("Error al obtener los datos del servicio:", error);
        setRequestError(error.message);
      });

    axios
      .get(
        `https://casac-com.onrender.com/response/reporter_nameSec/${region}/detail`
      )
      .then((response) => {
        const DataDetail = response.data;

        setSessionDataDetail(DataDetail);
      })
      .catch((error) => {
        console.error("Error al obtener los datos del servicio:", error);
        setRequestError(error.message);
      });
  }, []);

  useEffect(() => {
    if (sessionData.length > 0) {
      sessionData.forEach((group, index) => {
        const canvasId = `pentagon-chart-${index + 1}`;
        if (document.getElementById(canvasId)) {
          const formattedData = {
            labels: group.map((item) => obtenerIniciales(item.nombreSesion)),
            datasets: [
              {
                label: `Grupo ${index + 1}`,
                data: group.map((item) => parseFloat(item.promedio)),
                backgroundColor:
                  transparentColors[index % transparentColors.length],
                borderColor: colors[index % colors.length],
                borderWidth: 2.5,
              },
            ],
          };
          createOrUpdateRadarChart(canvasId, formattedData, options);
        } else {
          console.error(`Canvas with ID ${canvasId} not found`);
        }
      });
    }
  }, [sessionData]); // Dependencia en sessionData para asegurar recarga cuando cambie.

  const colors = [
    "#012B31",
    "#E95837",
    "#B3865E",
    "#F08235",
    "#9CC430",
    "#009940",
    "#4886C6",
    "#1B6E81",
    "#012B31",
  ]; // Colores para los gráficos
  const transparentColors = [
    "rgba(1, 43, 49, 0.5)",
    "rgba(233, 88, 55, 0.5)",
    "rgba(179, 134, 94, 0.5)",
    "rgba(240, 130, 53, 0.5)",
    "rgba(156, 196, 48, 0.5)",
    "rgba(0, 153, 64, 0.5)",
    "rgba(72, 134, 198, 0.5)",
    "rgba(27, 110, 129, 0.5)",
    "rgba(1, 43, 49, 0.5)",
  ];
  const options = {
    elements: {
      line: {
        borderWidth: 1,
      },
      point: {
        radius: 6,
      },
    },
    plugins: {
      legend: {
        display: false,
      },
      tooltip: {
        enabled: false,
      },
    },
    scales: {
      r: {
        suggestedMin: 0,
        suggestedMax: 100,
        grid: {
          color: "#000000c2",
        },
        pointLabels: {
          font: {
            size: "15vm",
          },
          color: "#012B31",
        },
        angleLines: {
          color: "#000000c2",
        },
        ticks: {
          color: "#012B31", // Cambiar el color de las marcas de los ejes
          beginAtZero: true,
          min: 0,
          max: 100, // Forzar que el máximo sea 100
        },
      },
    },
  };
  const handleDownloadReport = () => {
    let dataForExcel = [];

    if (!sessionDataDetail || !sessionDataDetail.formulariosEncontrados) {
      console.error("No se encontraron datos en sessionDataDetail");
      return [];
    }

    // Iterar sobre cada formulario encontrado
    sessionDataDetail.formulariosEncontrados.forEach((formulario) => {
      // Iterar sobre cada sesión del formulario
      formulario.sesiones.forEach((sesion) => {
        // Iterar sobre las respuestas de la sesión
        sesion.respuesta.forEach((respuesta) => {
          // Manejar respuestas que contienen un array de elementos
          if (respuesta.respuesta instanceof Array) {
            // Procesar cada elemento del array de respuesta
            respuesta.respuesta.forEach((element, index) => {
              if (typeof element === "object" && element !== null) {
                dataForExcel.push({
                  Sesión: sesion.nombre,
                  NombrePregunta: respuesta.nombre,
                  EtiquetaPregunta: element.etiqueta || `Subparte ${index}`,
                  Valor: element.valor,
                });
              }
            });
          } else if (
            respuesta.respuesta &&
            respuesta.respuesta.valor !== undefined
          ) {
            // Asumiendo que algunos valores son directamente accesibles
            let value = respuesta.respuesta.valor;
            dataForExcel.push({
              Sesión: sesion.nombre,
              NombrePregunta: respuesta.nombre,
              EtiquetaPregunta: "",
              Valor: value,
            });
          }
        });
      });
    });

    return dataForExcel;
  };

  const handleOpenModal = () => {
    setIsReportModalOpen(true);
  };
  return (
    <div>
      {activeComponent !== "Region" && (
        <div className="content_region_report">
          {requestError ? (
            <p>No se pudo obtener los datos del servidor: {requestError}</p>
          ) : (
            <div style={{ display: "flex", flexWrap: "wrap", gap: "20px" }}>
              <div className="Regresar_reports">
                <button className="export" onClick={handleOpenModal}>
                  Exportar Reporte
                </button>{" "}
                {/* Botón para abrir el modal */}
                {formData.length > 0 ? (
                  <button onClick={handleEditTransition}>
                    Editar ruta de trancición
                  </button>
                ) : (
                  <button onClick={handleSaveTransition}>
                    Guardar ruta de transición
                  </button>
                )}
                <button onClick={handleRegion} className="btn-volver">
                  Volver
                </button>
              </div>
              {sessionData.map((group, index) => (
                <div key={index + 1} className="content_region_report">
                  <div className="grafica">
                    <h3>{valores[index]}</h3>
                    <canvas id={`pentagon-chart-${index + 1}`}></canvas>
                    <div>
                      {group.map((element, idx) => (
                        <div key={idx + 1}>
                          <p>
                            <span
                              style={{ fontWeight: "bold", fontSize: "1.2em" }}
                            >
                              {idx + 1}:
                            </span>
                            {element.nombreSesion}:{" "}
                            <span
                              style={{
                                padding: "2px 5px",
                                borderRadius: "4px",
                                color: "white",
                                fontWeight: "bold",
                                backgroundColor:
                                  element.promedio <= 30
                                    ? "#E95837"
                                    : element.promedio <= 70
                                    ? "#F08235"
                                    : "#009940",
                              }}
                            >
                              {parseFloat(element.promedio).toFixed(2)}
                            </span>
                          </p>
                        </div>
                      ))}
                    </div>
                  </div>
                  <div className="results">
                    <div className="contentInter">
                      <h3>
                        Resultados de la Guía sesión de la Region: {region}
                      </h3>
                      <p>
                        Resultados: {calcularPorcentaje(sessionData[index])}%
                      </p>
                      <ProgressBar
                        color={ColorBarra(sessionData[index])} // Cambiado a className
                        value={calcularPorcentaje(sessionData[index])}
                        maxValue={100}
                      />
                    </div>
                    <div className="contentInter">
                      <h3>Lo que falta para llegar a la situación ideal</h3>
                      <p>
                        Resultados: {calcularPorcentajeOut(sessionData[index])}%
                      </p>
                      <ProgressBar
                        color={ColorBarraOut(sessionData[index])} // Cambiado a className
                        value={calcularPorcentajeOut(sessionData[index])}
                        maxValue={100}
                      />
                    </div>
                    <div className="contentInter">
                      <h3>Propuesta para ruta de transición agroecológica: </h3>
                      <p>Nota:</p>
                      <textarea
                        style={{ width: "100%", height: "200px" }}
                        placeholder="Ingrese aquí su propuesta..."
                        value={
                          transitionProposal[index]
                            ? transitionProposal[index]
                            : ""
                        }
                        onChange={(event) => handleProposalChange(event, index)}
                      />
                    </div>
                  </div>
                </div>
              ))}
            </div>
          )}

          <Modal
            isOpen={reportModalOpen}
            onRequestClose={handleCloseModal}
            contentLabel="Reporte Region"
            style={{
              overlay: {
                backgroundColor: "rgba(255, 255, 255, 0)", // Fondo transparente
              },
              content: {
                maxWidth: "400px",
                width: "30%", // Ancho del modal
                maxHeight: "100%",
                height: "14%", // Alto del modal
                margin: "auto", // Centrado horizontal
                padding: "20px", // Espacio alrededor del contenido
                backgroundColor: "white", // Color de fondo del modal
                overflowY: "auto", // Agregar scroll vertical si es necesario
                border: "none", // Eliminar borde
                borderRadius: "none", // Eliminar bordes redondeados
                boxShadow: "none", // Eliminar sombra
              },
            }}
          >
            <h2>Descargar Reporte Region</h2>
            <form>
              <div className="Btns_export">
                <ExportToExcel
                  chartData={handleDownloadReport()}
                  name={region}
                  type={"region"}
                />
                <button
                  className="close-button"
                  data-tooltip="Cerrar"
                  onClick={handleCloseModal}
                >
                  Cancelar
                </button>
              </div>
            </form>
          </Modal>
        </div>
      )}
      {activeComponent === "Region" && <Region />}
    </div>
  );
};

export default Region_Reporte;
