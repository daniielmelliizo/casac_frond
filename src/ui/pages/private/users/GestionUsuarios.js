// ui/pages/GestionUsuarios.js
import React from "react";
import "../../../styles/GestionUsuarios.css";
import Modal from "react-modal";
import UserList from "./UsersList";
Modal.setAppElement("#root");

const GestionUsuarios = () => {
  
  return (
    
      <div className="Users_main">
        <h2>Gestión de usuarios </h2>
        <UserList />
      </div>
  );
};

export default GestionUsuarios;
