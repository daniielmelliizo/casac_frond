// ui/pages/GestionUsuarios.js
import React, { useState, useEffect } from "react";
import "../../../styles/GestionUsuarios.css";
import Modal from "react-modal";
import { MdAdd } from "react-icons/md";
import UserService from "../../../../application/services/usersService";
import UserController from "../../../../interfaz/controllers/userController";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { show_alerta } from "../../../../function";
import SearchInput from "../../../components/app/SearchInput";
import IconButton from "../../../components/app/IconButton"
import SelectOptions from "../../../components/app/SelectedOptions";
import { ColorRing } from "react-loader-spinner";
import DataTable from "../../../components/app/DataTable";
import UserModal from "../../../components/app/UserModal";
import UserModalRespuesta from "../../../components/app/UserModalRespuesta";
Modal.setAppElement("#root");

const userService = new UserService();
const userController = new UserController(userService);

const GestionUsuarios = () => {
  const [searchTerm, setSearchTerm] = useState("");
  const [searchTermRol, setSearchTermRol] = useState("");
  const [users, setUsers] = useState([]);
  const [selectedRow, setSelectedRow] = useState(null);
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [isUsuarioModalOpen, setIsUsuarioModalOpen] = useState(false);
  const [isUsuarioModalOpenRespuesta, setIsUsuarioModalOpenRespuesta] = useState(false);
  const [usuarioid, setUsuarioid] = useState("");

  //generamos una funcion para la tabla y un filtro
  const currentPosts = users
    .filter((user) => {
      const fullName = `${user.nombres} ${user.apellidos}`.toLowerCase();
      const fullRol = `${user.rol}`.toLowerCase();

      return (
        fullName.includes(searchTerm.toLowerCase()) &&
        fullRol.includes(searchTermRol.toLowerCase())
      );
    })
    .map((user) => [
      user.id,
      user.nombres,
      user.apellidos,
      user.correo,
      user.contrasena,
      user.identificacion,
      user.universidad,
      user.profesion,
      user.grupo_investigacion,
      user.rol,
      user.ultimo_acceso,
      user.status,
    ]);

  const [formData, setFormData] = useState({
    nombres: "",
    apellidos: "",
    correo: "",
    contrasena: "",
    identificacion: "",
    universidad: "",
    profesion: "",
    grupo_investigacion: "",
    rol: "",
    ultimo_acceso: new Date().toLocaleDateString("es-ES"),
    status: "Activo",
  });

  //consumo para traer los datos
  const handleGetUserss = async () => {
    try {
      const response = await userController.handleGetUsers();
      const usersData = response.usuarios || []; // Extract the array from the object
      setUsers(usersData);
      setLoading(false);
    } catch (error) {
      setError(error.message);
      setLoading(false);
      console.error("Error al obtener los usuarios:", error.message);
    }
  };
  useEffect(() => {
    const handleGetUserss = async () => {
      try {
        const response = await userController.handleGetUsers();
        const usersData = response.usuarios || []; // Extract the array from the object
        setUsers(usersData);
        setLoading(false);
      } catch (error) {
        setError(error.message);
        setLoading(false);
        console.error("Error al obtener los usuarios:", error.message);
      }
    };

    handleGetUserss();
  }, []);
  //abrir modal crear usuario
  const usuario = (row) => {
    setSelectedRow(row);
    setIsUsuarioModalOpen(true);
  };
  //limpieza de datos formulario
  const clearForm = () => {
    setFormData({
      nombres: "",
      apellidos: "",
      identificacion: "",
      universidad: "",
      profesion: "",
      grupo_investigacion: "",
      rol: "",
      correo: "",
      contrasena: "",
      ultimo_acceso: new Date().toLocaleDateString("es-ES"),
      status: "Activo",
    });
  };

  const contieneError = (frase) => {
    // Convertir a cadena si no es una cadena
    if (typeof frase !== 'string') {
      frase = String(frase);
    }

    let fraseMinusculas = frase.toLowerCase();
    return fraseMinusculas.includes("error");
  };
  //consumo para crear los datos
  const handleRegistro = async () => {
    if (
      formData.nombres === "" ||
      formData.apellidos === "" ||
      formData.universidad === "" ||
      formData.profesion === "" ||
      formData.grupo_investigacion === "" ||
      formData.contrasena === ""
    ) {
      show_alerta("Faltan campos por llenar", "error");
    } else if (!validateIdentification(formData.identificacion)) {
      show_alerta(
        "La indentificacion debe tener entre 6 y 10 digitos",
        "error"
      );
    } else if (!validateEmail(formData.correo)) {
      show_alerta("El correo electrónico no es válido.", "error");
    } else {
      try {
        console.log("Info usarios", formData);
        const respuesta = await userController.handleCreateUser(formData);
        // Cerrar el modal
        setIsUsuarioModalOpen(false);
        handleGetUserss();
        setDatos();
        if (contieneError(respuesta)) {
          // Si la respuesta incluye el mensaje de error específico, muestra una alerta de error
          show_alerta(respuesta, "error");
        } else {
          // Si la respuesta no incluye el mensaje de error específico, muestra una alerta de éxito
          clearForm();
          console.log(respuesta);
          show_alerta(respuesta, "success");
        }
      } catch (error) {
        show_alerta(error.message, "error");
      }
    }
  };
  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  //consumo para editar

  // validaicones registro
  const validateEmail = (email) => {
    // Expresión regular para validar correos electrónicos
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return regex.test(email);
  };
  const validateIdentification = (identification) => {
    return /^[0-9]{6,10}$/.test(identification);
  };
  const handleEditModal = async () => {
    if (
      formData.nombres === "" ||
      formData.apellidos === "" ||
      formData.universidad === "" ||
      formData.profesion === "" ||
      formData.grupo_investigacion === "" 
      
    ) {
      show_alerta("Faltan campos por llenar", "error");
      return;
    } else if (!validateIdentification(formData.identificacion)) {
      show_alerta(
        "La indentificacion debe tener entre 6 y 10 digitos",
        "error"
      );
      return;
    } else if (!validateEmail(formData.correo)) {
      show_alerta("El correo electrónico no es válido.", "error");
      return;
    }
    try {
      const respuesta = await userController.handleUpdateUsers(
        selectedRow.id,
        formData
      );
      setDatos();
      setIsEditModalOpen(false);
      handleGetUserss();
      if (respuesta) {
        show_alerta("Usuario actualizado exitosamente ", "success");
      } else {
        show_alerta(
          "Error al editar el usuario. Por favor, inténtalo de nuevo.",
          "error"
        );
      }
    } catch (error) {
      show_alerta(
        "Error al editar el usuario. Por favor, inténtalo de nuevo.",
        "error"
      );
    }
  };
  const handleEdit = (user) => {
    console.log(JSON.stringify(user[0]))
    setSelectedRow(user);
    setFormData({
      id: user[0],
      nombres: user[1],
      apellidos: user[2],
      correo: user[3],
      identificacion: user[5],
      universidad: user[6],
      profesion: user[7],
      grupo_investigacion: user[8],
      rol: user[9],
      ultimo_acceso: user[10],
      status: user[11],
    });
    console.log(formData)
    setIsEditModalOpen(true);
  };

  const handleInputChangeEditModal = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  //consumo para eliminar
  const handleDelete = async (row) => {
    try {
      const MySwal = withReactContent(Swal);
      MySwal.fire({
        title: "Seguro de eliminar el usuario  " + row[1] +" "+ row[2] + "  ?",
        icon: "question",
        text: "no hay marcha atrás",
        showCancelButton: true,
        confirmButtonText: "Eliminar",
        cancelButtonText: "Cancelar",
        customClass:{
          confirmButton:'modify-button-class',
          cancelButton:'cancel-button-class'
        },
        buttonsStyling: false,
      }).then(async (result) => {
        try {
          if (result.isConfirmed) {
            console.log("Antes de eliminar usuario:", row);
            await userController.handleDeleteUser(row[0]);
            console.log("Después de eliminar usuario");
            await handleGetUserss();
            console.log("Después de actualizar usuarios en la tabla");
            show_alerta("El usuario fue eliminado", "success");
          } else {
            show_alerta("El Usuario No fue eliminado", "success");
          }
        } catch (error) {
          show_alerta("Error al ejecutar la acción", "error");
          console.error("Error en handleDelete:", error);
        }
      });
    } catch (error) {
      show_alerta("Error en la solicitud", "error");
      console.error("Error en handleDelete:", error);
    }
  };

  const handleSearch = (e) => {
    setSearchTerm(e.target.value);
  };
  const handleSearchRol = (e) => {
    setSearchTermRol(e.target.value);
  };

  const setDatos = () => {
    setFormData({
      nombres: "",
      apellidos: "",
      correo: "",
      contrasena: "",
      identificacion: "",
      universidad: "",
      profesion: "",
      grupo_investigacion: "",
      rol: "",
      ultimo_acceso: new Date().toLocaleDateString("es-ES"),
      status: "Activo",
    });
  }
  const handleForm = async (row) => {
    setIsUsuarioModalOpenRespuesta(true);
    setUsuarioid(row[0]);
  }

  //cerrar la modal
  const handleCloseModal = () => {
    clearForm();
    setIsEditModalOpen(false);
    setIsUsuarioModalOpen(false);
    setDatos();
    setIsUsuarioModalOpenRespuesta(false);
  };

  if (loading) {
    return <div className="loading-container"><ColorRing
    visible={true}
    height="10"
    width="10"
    ariaLabel="blocks-loading"
    wrapperStyle={{}}
    wrapperClass="blocks-wrapper"
    colors={['#e15b64', '#f47e60', '#f8b26a', '#abbd81', '#849b87']}
  /> <h3>cargando ....</h3> </div>;
  }
  if (error) {
    return <p>Error: {error}</p>;
  }
  if (!Array.isArray(users)) {
    console.error("usuarios is not an array:", users);
    return <p>Error: Guias is not an array</p>;
  }

  const rolesOptions = [
    { label: 'Selecciona...', value: '' },
    { label: 'Investigador', value: 'investigador' },
    { label: 'Promotor', value: 'promotor' },
    { label: 'Productor', value: 'productor' },
  ];
  const headers = ['usuario', 'Email', 'Ultimo Acceso', 'Rol', 'Opciones'];
  const visibles = [1, 2, 3, 10, 9]
  return (
    <div className="content_u">
      <div className="bar_search_u">
        <SearchInput searchTerm={searchTerm} handleSearch={handleSearch} />
        <div className="create">
          <IconButton text="Crear usuario" icon={<MdAdd size={45} />} onClick={usuario} className="btn_crear" />
        </div>
        <div className="order">
          <SelectOptions id="selectOption" value={searchTermRol} onChange={handleSearchRol} options={rolesOptions} />
        </div>
      </div>
      <DataTable
        headers={Array.isArray(headers) ? headers : []}
        visibles={visibles}
        rows={Array.isArray(currentPosts) ? currentPosts : []}
        renderCell={(cell, rowIndex, cellIndex, row) => {
          return cell;
        }}
        handleDelete={handleDelete}
        handleEdit={handleEdit}
        handleForm={handleForm}
        type={"users"}
      />
      <UserModal
        isOpen={isEditModalOpen}
        onRequestClose={handleCloseModal}
        contentLabel="Editar Usuario"
        formData={formData}
        handleInputChangeEditModal={handleInputChangeEditModal}
        handleEditModal={handleEditModal}
        handleCloseModal={handleCloseModal}
      />
      <UserModal
        isOpen={isUsuarioModalOpen}
        onRequestClose={handleCloseModal}
        contentLabel="Agregar Usuario"
        formData={formData}
        handleInputChange={handleInputChange}
        handleRegistro={handleRegistro}
        handleCloseModal={handleCloseModal}
      />
      <UserModalRespuesta
      isOpen={isUsuarioModalOpenRespuesta}
      onRequestClose={handleCloseModal}
      titulo="Formularios registrados"
      handleCloseModal={handleCloseModal}
      idUsers = {usuarioid}
      />
    </div>
  );
};

export default GestionUsuarios;
