import React, { useState, useEffect } from "react";
import { BsXCircleFill } from "react-icons/bs";
import { FaAngleDown, FaAngleRight } from "react-icons/fa";
import { Link, useLocation } from "react-router-dom";
import DatePicker from "react-datepicker";
import TimePicker from "react-time-picker";
import ContentIcon from "../../../components/app/ContentIcon";
import ContentInput from "../../../components/app/Input";
import ListaSelect from "../../../components/app/ListSelect";
import CheckboxGroup from "../../../components/app/CheckButton";
import RadioGroup from "../../../components/app/RadioButton";
import CustomTextarea from "../../../components/app/TextArea";
import MatrixComponent from "../../../components/app/Matrix";
import "react-datepicker/dist/react-datepicker.css";
import "react-time-picker/dist/TimePicker.css";
import "../../../styles/PrevisualForm.css";
import FormService from "../../../../application/services/formService";
import FormController from "../../../../interfaz/controllers/formControler";

const formService = new FormService();
const formControler = new FormController(formService);

const PrevisualForm = () => {
  const location = useLocation();
  const idformulario = location?.state?.idform || "idform";
  const [startDate, setStartDate] = useState(new Date()); // Add this line for 'fecha'
  const [hora, setHora] = useState("12:00"); // Add this line for 'hora'

  const [formData, setFormData] = useState({
    nombreFormulario: "",
    descripcionFormulario: "",
  });

  const [formState, setFormState] = useState({
    secciones: [],
  });

  const toggleSection = (sectionIndex) => {
    const updatedFormState = { ...formState };
    updatedFormState.secciones[sectionIndex].isOpen =
      !updatedFormState.secciones[sectionIndex].isOpen;
    setFormState(updatedFormState);
  };

  const renderCampo = (campo, questionIndex) => {
    if (!campo) {
      return null;
    }
    switch (campo.tipo) {
      case "texto":
      case "correo":
        return (
          <ContentInput
            inputType={campo.tipo}
            placeholder={campo.placeholder}
          />
        );
      case "lista":
        return <ListaSelect questionIndex={questionIndex} campo={campo} />;
      case "matriz":
        return (
          <MatrixComponent campo={campo} questionIndex={questionIndex} />
        );

      case "decimal":
        return (
          <ContentInput
            inputType={campo.tipo}
            placeholder={campo.placeholder}
          />
        );

      case "numerico":
        return (
          <ContentInput
            inputType={campo.tipo}
            placeholder={campo.placeholder}
          />
        );

      case "checkbox":
        return (
          <CheckboxGroup
          questionIndex={questionIndex}
          campo={campo}
          onCheckboxChange={campo.valor}
        />
        );
      case "opcion_unica":
        return (
          
          <RadioGroup
            questionIndex={questionIndex}  // Ajusta según tus necesidades
            campo={campo}  // Ajusta según tus necesidades
            onRadioChange={campo.valor}  // Ajusta según tus necesidades
        />

        );
      case "fecha":
        return (
          <DatePicker
            key={questionIndex}
            selected={startDate} // Usa un estado para manejar la fecha seleccionada
            onChange={(date) => setStartDate(date)}
            placeholderText={campo.placeholder}
            required={campo.requerido}
          />
        );
      case "hora":
        return (
          <TimePicker
            key={questionIndex}
            value={hora} // Usa un estado para manejar la hora seleccionada
            onChange={(time) => setHora(time)}
            required={campo.requerido}
          />
        );
      case "textarea":
        return (
          <CustomTextarea
          key={questionIndex}
          questionIndex={questionIndex}
          campo={campo}
        />
        );
      case "foto":
      case "audio":
      case "gps":
      case "firma":
        return (
          <ContentIcon
            contentType={campo.tipo}
            placeholder={campo.placeholder}
          />
        );
      default:
        return null;
    }
  };

  const fetchData = async () => {
    try {
      if (idformulario) {
        const guia = await formControler.handleConsultarForm(idformulario);

        if (
          guia.formulario.nombreFormulario &&
          guia.formulario.descripcionFormulario
        ) {
          setFormData({
            nombreFormulario: guia.formulario.nombreFormulario,
            descripcionFormulario: guia.formulario.descripcionFormulario,
          });
        }

        if (
          guia &&
          guia.formulario &&
          guia.formulario.sesiones &&
          Array.isArray(guia.formulario.sesiones) &&
          guia.formulario.sesiones.length > 0
        ) {
          const sesionesTotales = guia.formulario.sesiones.map((sesion) => {
            return {
              titulo: sesion.nombre,
              preguntas: sesion.campos.map((campo) => {
                return {
                  tipo: campo.tipo,
                  etiqueta: campo.etiqueta,
                  placeholder: campo.placeholder,
                  opciones: (() => {
                    if (campo.tipo === "opcion_unica") {
                      return campo.opciones
                        ? campo.opciones.map((opcion) => ({
                          etiqueta: opcion.etiqueta,
                          valor: opcion.valor,
                        }))
                        : [];
                    } else if (campo.tipo === "matriz") {
                      return {
                        filas: campo.filas,
                        columnas: campo.columnas,
                      };
                    } else {
                      return campo.opciones
                        ? campo.opciones.map((opcion) => ({
                            etiqueta: opcion.etiqueta,
                            valor: opcion.valor,
                          }))
                        : [];
                    }
                  })(),
                };
              }),
            };
          });

          setFormState({
            secciones: sesionesTotales,
          });
        } else {
          console.warn(
            "Datos incompletos o incorrectos en la respuesta del servicio:",
            guia
          );
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <div className="container-body-prev">
        <div className="container-body-button">
          <Link className="link" to="/despliegues">
            <button className="actions" data-tooltip="ATRAS">
              <BsXCircleFill />
            </button>
          </Link>
        </div>
        <h1>{formData.nombreFormulario}</h1>
        <h2>{formData.descripcionFormulario}</h2>
        <div className="form-container">
          {formState.secciones.map((seccion, sectionIndex) => (
            <div key={sectionIndex} className="form-section">
              <div
                className="section-header"
                onClick={() => toggleSection(sectionIndex)}
              >
                <p style={{ margin: "10px" }}>{seccion.titulo} </p>
                
                <span style={{ margin: "10px" }}>
                  {seccion.isOpen ? <FaAngleDown /> : <FaAngleRight />}
                </span>
              </div>
              {seccion.isOpen && (
                <div className="form-questions">
                  {seccion.preguntas.map((question, questionIndex) => (
                    <div key={questionIndex} className="form-question">
                      <p>{question.etiqueta}:</p>
                      {renderCampo(question, questionIndex)}
                    </div>
                  ))}
                </div>
              )}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default PrevisualForm;
