import React, { useState } from "react";
import {useNavigate } from "react-router-dom";
import Modal from "react-modal";
import GuiasList from "./GuiasList";
import "../../../styles/Guias.css";
import GuiaService from "../../../../application/services/guiaService";
import GuiaController from "../../../../interfaz/controllers/guiaController";
import { show_alerta } from "../../../../function";

Modal.setAppElement("#root");
const guiaService = new GuiaService();
const guiaController = new GuiaController(guiaService);

const Guias = () => {
  const navigate = useNavigate();
  const [isGuiaModalOpen, setIsGuiaModalOpen] = useState(false);
  
  const [formData, setFormData] = useState({
    id: "",
    nombreFormulario: "",
    descripcionFormulario: "",
    fechaCreacion: new Date().toLocaleDateString("es-ES"),
    fechaActualizacion: new Date().toLocaleDateString("es-ES"),
    status: "inactiva"
  });

  const handleRegistro = async () => {
    if (!formData.nombreFormulario || !formData.descripcionFormulario) {
      show_alerta("Por favor, llena todos los campos.", "warning");
      return;
    }
    try {
      const respuesta = await guiaController.handleCreateGuia(formData);

      setIsGuiaModalOpen(false);

      if (respuesta) {
        show_alerta("Guía creada exitosamente. ", "success");
        // Redirige a la página de caracterización con el título de la guía
        navigate(`/caracterizacion`, {
          state: { idform: respuesta.guiaId }   ,
        });
      } else {
        show_alerta(
          "Error al crear la guía. Por favor, inténtalo de nuevo.",
          "error"
        );
      }
    } catch (error) {
      console.error("Error al registrar guía:", error.message);
      show_alerta(
        "Error al crear la guía. Por favor, inténtalo de nuevo.",
        "error"
      );
    }
  };

  

  const handleCloseModal = () => {
    setIsGuiaModalOpen(false);
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const Guia = (row) => {
    setIsGuiaModalOpen(true);
  };

  const hoy = new Date().toLocaleDateString("es-ES", {
    weekday: "long",
    day: "numeric",
    month: "long",
    year: "numeric",
  });
  const nameUser = localStorage.getItem("name");
  return (
    <div className="content-guia-p">
      <div className="content2">
        <h3>
          Bienvenido, {nameUser}. Es {hoy}{" "}
        </h3>
        
        <GuiasList guiaData={Guia}/>
        
      </div>
      <Modal
        isOpen={isGuiaModalOpen}
        onRequestClose={handleCloseModal}
        contentLabel="Agregar Guía"
        style={{
          overlay: {
            backgroundColor: "rgba(255, 255, 255, 0)", // Fondo transparente
          },
          content: {
            maxWidth:"800px",
            width: "30%", // Ancho del modal
            maxHeight:"100%",
            height: "38%", // Alto del modal
            margin: "auto", // Centrado horizontal
            padding: "20px", // Espacio alrededor del contenido
            backgroundColor: "white", // Color de fondo del modal
            overflowY: "auto", // Agregar scroll vertical si es necesario
            border: "none", // Eliminar borde
            borderRadius: "none", // Eliminar bordes redondeados
            boxShadow: "none", // Eliminar sombra
          },
        }}
      >
        <div className="Modal_guias_content">
        <h2>Registro nueva guía</h2>
            <label className="title" htmlFor="nombreFormulario">
              Título de la Guía
            </label>
            <input
              type="text"
              id="nombreFormulario"
              name="nombreFormulario"
              value={formData.nombreFormulario}
              onChange={handleInputChange}
              required
            />
            <label className="title" htmlFor="descripcionFormulario">
              Descripción
            </label>
            <input
              type="text"
              id="descripcionFormulario"
              name="descripcionFormulario"
              value={formData.descripcionFormulario}
              onChange={handleInputChange}
              required
            />
            <div className="btn_a_guia">
            <button
              className="create-button"
              type="button"
              onClick={handleRegistro} 
            >
              Guardar
            </button>

            <button
              className="close-button"
              type="button"
              onClick={handleCloseModal}
            >
              Cerrar
            </button>
            </div>
          </div>
      </Modal>
    </div>
  );
};

export default Guias;
