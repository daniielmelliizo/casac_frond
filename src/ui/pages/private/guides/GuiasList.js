import React, { useState, useEffect } from "react";
import { FaEdit, FaTrash, FaSearch } from "react-icons/fa";
import { MdAdd } from "react-icons/md";
import "../../../styles/GuiasList.css";
import { useNavigate } from "react-router-dom";
import GuiaService from "../../../../application/services/guiaService";
import GuiaController from "../../../../interfaz/controllers/guiaController";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { show_alerta } from "../../../../function";
import { ColorRing } from  'react-loader-spinner';

const guiaService = new GuiaService();
const guiaController = new GuiaController(guiaService);

const GuiasList = ({ guiaData }) => {
  const navigate = useNavigate();
  const [guias, setGuias] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  
  const [searchTerm, setSearchTerm] = useState("");
  const currentPosts = guias
  .filter((guia) => {
    const fullName = `${guia.nombreFormulario}`.toLowerCase();
    
    return fullName.includes(searchTerm.toLowerCase());
  });
  const [selectedOption, setSelectedOption] = useState("");
  const handleSearch = (e) => {
    setSearchTerm(e.target.value);
  };
  const handleSelectChange = (event) => {
    setSelectedOption(event.target.value);
  };
  const handleGetGuias = async () => {
    try {
      const response = await guiaController.handleGetGuias();
      const guiasData = response.formularios || []; // Extract the array from the object
      setGuias(guiasData);
      setLoading(false);
    } catch (error) {
      setError(error.message);
      setLoading(false);
    }
  };
  useEffect(() => {
    const handleGetGuias = async () => {
      try {
        const response = await guiaController.handleGetGuias();
        const guiasData = response.formularios || [] ; // Extract the array from the object
        setGuias(guiasData);
        // console.log("prueba: " , guiasData);
        setLoading(false);
      } catch (error) {
        setError(error.message);
        setLoading(false);
      }
    };

    handleGetGuias();
  }, []);

  const handleModificar = (id, nombreFormulario) => {
    const MySwal = withReactContent(Swal);
    MySwal.fire({
      title: `¿Estás seguro de modificar la Guía : ${nombreFormulario}  ?`,
      icon: "question",
      showCancelButton: true,
      confirmButtonText: "Modificar",
      cancelButtonText: "Cancelar",
      customClass:{
        confirmButton:'modify-button-class',
        cancelButton:'cancel-button-class'
      },
      buttonsStyling: false,
    }).then((result) => {
      if (result.isConfirmed) {
        navigate(`/caracterizacion`, { state: { idform: id } });
      }
    });
  };

  const handleEliminar = (id, nombreFormulario) => {
    const MySwal = withReactContent(Swal);
    MySwal.fire({
      title: `¿Estás seguro de eliminar la Guía : ${nombreFormulario}  ?`,
      icon: "question",
      text: "Si se elimina la guía por ende las sesiones correspondientes a la misma",
      showCancelButton: true,
      confirmButtonText: "Eliminar",
      cancelButtonText: "Cancelar",
      customClass:{
        confirmButton:'modify-button-class',
        cancelButton:'cancel-button-class'
      },
      buttonsStyling: false,
      
    }).then(async (result) => {
      if (result.isConfirmed) {
        show_alerta("La Guía fue eleminada", "success");
        await guiaController.handleDeleteGuia(id);
        handleGetGuias();
      } else {
        show_alerta("La Guía No fue eliminado", "info");
      }
    });
  };

  if (loading) {
    return <div><ColorRing
    visible={true}
    height="80"
    width="80"
    ariaLabel="blocks-loading"
    wrapperStyle={{}}
    wrapperClass="blocks-wrapper"
    colors={['#e15b64', '#f47e60', '#f8b26a', '#abbd81', '#849b87']}
  /> <h3>cargando ....</h3> </div>;
  }

  if (error) {
    return <p>Error: {error}</p>;
  }

  if (!Array.isArray(guias)) {
    return <p>Error: Guias is not an array</p>;
  }

  return (
    <div className="container_full">
      <div className="search-and-create">
        <div className="search-bar">
          <input
            type="text"
            value={searchTerm}
            onChange={handleSearch}
            placeholder="Buscar..."
          />
          <FaSearch className="search-icon" />
        </div>
        <button
          className="btn_crear"
          onClick={guiaData}
        >
          <MdAdd size={20} />
          <span>Crear Guía</span>
        </button>

        <div className="order-by">
          <p>Ordenar por</p>
          <select
            id="selectOption"
            value={selectedOption}
            onChange={handleSelectChange}
          >
            <option value="">Selecciona...</option>
            <option value="opcion1">Opción 1</option>
            <option value="opcion2">Opción 2</option>
            <option value="opcion3">Opción 3</option>
          </select>
        </div>
      </div>
      <div className="guias-list-container">
        {currentPosts.map((guia) => (
          <div key={guia.id} className="load-wrapp">
            <div className="guia-header">
              <h1>{guia.nombreFormulario}</h1>
              <div className="icons">
                <button
                  style={{
                    display: "flex",
                    alignItems: "center",
                    background: "white",
                  }}
                  className={`icon icon-edit`}
                  data-tooltip="Modificar"
                  onClick={() => handleModificar(guia.id, guia.nombreFormulario)}
                >
                  <FaEdit />
                </button>
                <button
                  style={{
                    display: "flex",
                    alignItems: "center",
                    background: "white",
                  }}
                  className={`icon icon-trash`}
                  data-tooltip="Eliminar"
                  onClick={() => handleEliminar(guia.id, guia.nombreFormulario)}
                >
                  <FaTrash />
                </button>
              </div>
            </div>
            <p>{`Descripcion: ${guia.descripcionFormulario}`}</p>
            <p>{`Creado el: ${guia.fechaCreacion}`}</p>
            <p>{`Última modificación: ${guia.fechaActualizacion}`}</p>
          </div>
        ))}
      </div>
    </div>
  );
};

export default GuiasList;
