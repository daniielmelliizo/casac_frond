import React, { useState } from "react";
import {
  BsFillCheckCircleFill,
  BsXCircleFill,
  BsFillPlusCircleFill,
  BsCircleSquare,
  BsListTask,
  Bs123,
  BsTrash2,
  BsGeoAltFill,
} from "react-icons/bs";
import { PiSignatureDuotone } from "react-icons/pi";
import { CiViewTable } from "react-icons/ci";
import {
  MdOutlineInsertPhoto,
  MdOutlineAccessTime,
  MdOutlineDateRange,
  MdCheckCircleOutline,
  MdRadioButtonChecked,
} from "react-icons/md";
import { AiOutlineSound } from "react-icons/ai";
import { FaSync, FaAngleDown, FaAngleRight } from "react-icons/fa";
import { TbFileDescription } from "react-icons/tb";
import { Link, useLocation } from "react-router-dom";
import "../../../styles/GuiasCaract.css";
import FormService from "../../../../application/services/formService";
import FormController from "../../../../interfaz/controllers/formControler";
import { useEffect } from "react";
import DatePicker from "react-datepicker";
import TimePicker from "react-time-picker";
import ContentIcon from "../../../components/app/ContentIcon";
import "react-datepicker/dist/react-datepicker.css";
import "react-time-picker/dist/TimePicker.css";
import { show_alerta } from "../../../../function";
import { useNavigate } from "react-router-dom";
import Modal from "react-modal";
import ModalRegistroGuia from "../../../components/app/ModalRegistroGuia";

const formService = new FormService();
const formControler = new FormController(formService);

const GuiasCaract = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const idformulario = location?.state?.idform || "idform";
  const [startDate, setStartDate] = useState(new Date()); // Add this line for 'fecha'
  const [hora, setHora] = useState("12:00"); // Add this line for 'hora'
  const [showCampos, setShowCampos] = useState(false);
  const [modalOpenSesion, setModalOpenSesion] = useState(false);
  const [nombreSesion, setNombreSesion] = useState("");
  const [descripcionSesion, setDescripcionSesion] = useState("");
  const [focusedSection, setFocusedSection] = useState(null);
  const [indexSesion, setIndexSesion] = useState(null);
  const [modalOpenCampos, setModalOpenCampos] = useState(false);
  const [sectionIndex, setSectionIndex] = useState(null);
  const [campoOptions, setCampoOptions] = useState("");
  const [arrayCampoOptions, setArrayCampoOptions] = useState([]);
  const [filasOption, setFilasOption] = useState("");
  const [arrayFilasOption, setArrayFilasOption] = useState([]);
  const [columnasOption, setColumnasOption] = useState("");
  const [arrayColumnasOption, setArrayColumnasOption] = useState([]);
  const [preguntaQuiestion, setPreguntaQuiestion] = useState([]);
  const [valorTotal, setValorTotal] = useState(1);
  const [tipoCampo, setTipoCampo] = useState({
    tipo: "",
    etiqueta: "",
    placeholder: "",
    nombre: "",
    requerido: "",
    opciones: [],
    filas: [],
    columnas: [],
  });
  const [tiposDeCampos, setTiposDeCampos] = useState([
    { tipo: "texto", icono: <BsFillCheckCircleFill />, etiqueta: "Texto" },
    { tipo: "lista", icono: <BsCircleSquare />, etiqueta: "Lista" },
    { tipo: "numerico", icono: <Bs123 />, etiqueta: "Numérico" },
    { tipo: "correo", icono: <BsGeoAltFill />, etiqueta: "Correo" },
    { tipo: "decimal", icono: <Bs123 />, etiqueta: "Decimal" },
    { tipo: "fecha", icono: <MdOutlineDateRange />, etiqueta: "Fecha" },
    { tipo: "hora", icono: <MdOutlineAccessTime />, etiqueta: "Hora" },
    { tipo: "textarea", icono: <TbFileDescription />, etiqueta: "Descripción" },
    // { tipo: "foto", icono: <MdOutlineInsertPhoto />, etiqueta: "Foto" },
    // { tipo: "audio", icono: <AiOutlineSound />, etiqueta: "Audio" },
    { tipo: "gps", icono: <BsGeoAltFill />, etiqueta: "GPS" },
    // { tipo: "firma", icono: <PiSignatureDuotone />, etiqueta: "Firma" },
    { tipo: "matriz", icono: <CiViewTable />, etiqueta: "Matriz" },
    { tipo: "checkbox", icono: <MdCheckCircleOutline />, etiqueta: "checkbox" },
    {
      tipo: "opcion_unica",
      icono: <MdRadioButtonChecked />,
      etiqueta: "Opción Unica",
    },
  ]);
  const [formState, setFormState] = useState({
    nombreFormulario: "",
    descripcionFormulario: "",
    fechaCreacion: "",
    fechaActualizacion: "",
    status: "",
    sesiones: [],
  });
  const [isOpenState, setIsOpenState] = useState(
    Array(formState.sesiones.length).fill(false)
  );
  const fetchData = async () => {
    try {
      if (idformulario) {
        const sesion = await formControler.handleConsultarForm(idformulario);

        if (
          sesion.formulario.nombreFormulario &&
          sesion.formulario.descripcionFormulario &&
          sesion.formulario.fechaCreacion &&
          sesion.formulario.fechaActualizacion &&
          sesion.formulario.descripcionFormulario
        ) {
          setFormState((prevState) => ({
            ...prevState,
            nombreFormulario: sesion.formulario.nombreFormulario,
            descripcionFormulario: sesion.formulario.descripcionFormulario,
            fechaCreacion: sesion.formulario.fechaCreacion,
            fechaActualizacion: sesion.formulario.fechaActualizacion,
            status: sesion.formulario.status,
          }));
        }

        if (
          sesion &&
          sesion.formulario &&
          sesion.formulario.sesiones &&
          Array.isArray(sesion.formulario.sesiones) &&
          sesion.formulario.sesiones.length > 0
        ) {
          const sesionesTotales = sesion.formulario.sesiones.map((sesion) => {
            return {
              nombre: sesion.nombre,
              descripcion: sesion.descripcion,
              campos: sesion.campos.map((campo) => {
                const nuevoCampo = {
                  tipo: campo.tipo,
                  etiqueta: campo.etiqueta,
                  nombre: campo.nombre,
                  requerido: campo.requerido,
                  placeholder: campo.placeholder,
                  filas: campo.filas,
                  columnas: campo.columnas,
                };

                if (campo.tipo === "opcion_unica") {
                  nuevoCampo.opciones = campo.opciones
                    ? campo.opciones.map((opcion) => ({
                        etiqueta: opcion.etiqueta,
                        valor: opcion.valor,
                      }))
                    : [];
                } else if (campo.tipo === "matriz") {
                  nuevoCampo.filas = campo.filas.map((fila) => ({
                    etiqueta: fila.etiqueta,
                    valor: fila.valor,
                  }));
                  nuevoCampo.columnas = campo.columnas.map((columna) => ({
                    etiqueta: columna.etiqueta,
                    valor: columna.valor,
                  }));
                } else if (
                  campo.tipo !== "texto" &&
                  campo.tipo !== "correo" &&
                  campo.tipo !== "decimal" &&
                  campo.tipo !== "numerico"
                ) {
                  nuevoCampo.opciones = campo.opciones
                    ? campo.opciones.map((opcion) => ({
                        etiqueta: opcion.etiqueta,
                        valor: opcion.valor,
                      }))
                    : [];
                }

                return nuevoCampo;
              }),
            };
          });

          // Agregar información adicional al formulario antes de las sesiones
          const formularioCompleto = {
            nombreFormulario: sesion.formulario.nombreFormulario,
            descripcionFormulario: sesion.formulario.descripcionFormulario,
            fechaCreacion: sesion.formulario.fechaCreacion,
            fechaActualizacion: sesion.formulario.fechaActualizacion,
            status: sesion.formulario.status,
            sesiones: sesionesTotales,
          };

          setFormState({
            ...formularioCompleto,
          });
        } else {
          console.warn(
            "Datos incompletos o incorrectos en la respuesta del servicio:",
            sesion
          );
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);
  const addSesion = async () => {
    //abrir modal
    setModalOpenSesion(true);
  };
  const handleModalClose = () => {
    // Cierra el modal y restablece los estados
    setModalOpenSesion(false);
    setNombreSesion("");
    setDescripcionSesion("");
    // cierre de modal y restablece los estados de los campos
    setModalOpenCampos(false);
    setTipoCampo("");
    setArrayCampoOptions([]);
    setArrayFilasOption([]);
    setArrayColumnasOption([]);
  };

  const handleGuardarSesion = () => {
    if (!nombreSesion || !descripcionSesion) {
      show_alerta("todos los campos son obligatorios", "warning");
      return;
    }
    // Crea un objeto nuevaSesion con los datos ingresados y actualiza el estado del formulario
    const nuevaSesion = {
      nombre: nombreSesion,
      descripcion: descripcionSesion,
      campos: [],
      isOpen: true,
    };
    if (indexSesion !== null) {
      // Si estamos modificando una sesión existente, actualizamos esa sesión en el estado del formulario
      const updatedFormState = { ...formState };
      const sesionActualizada = {
        ...updatedFormState.sesiones[indexSesion],
        nombre: nombreSesion,
        descripcion: descripcionSesion,
        isOpen: true,
      };
      updatedFormState.sesiones[indexSesion] = sesionActualizada;
      setFormState(updatedFormState);
      setFocusedSection(sesionActualizada.nombre);
    } else if (indexSesion === null) {
      const updatedFormState = { ...formState };
      updatedFormState.sesiones.push(nuevaSesion);
      setFormState(updatedFormState);
    }

    // Cierra el modal y restablece los estados
    setModalOpenSesion(false);
    setNombreSesion("");
    setDescripcionSesion("");
    setModalOpenCampos(false);
  };
  const modificarSesion = async (sectionIndex) => {
    // Crear una copia del estado del formulario y la sesión actual
    const updatedFormState = { ...formState };
    const sesionActual = { ...updatedFormState.sesiones[sectionIndex] };
    // Establecer los estados necesarios para la modal
    setModalOpenSesion(true);
    setIndexSesion(sectionIndex);
    setNombreSesion(sesionActual.nombre);
    setDescripcionSesion(sesionActual.descripcion);
  };
  const removeSesion = (sectionIndex) => {
    const updatedFormState = { ...formState };
    updatedFormState.sesiones.splice(sectionIndex, 1);
    setFormState(updatedFormState);
  };
  const eliminarCampo = (sectionIndex, questionIndex) => {
    const updatedFormState = { ...formState };
    updatedFormState.sesiones[sectionIndex].campos.splice(questionIndex, 1);
    setFormState(updatedFormState);
  };
  const agregarCampo = async (tipoCampo, sectionIndex) => {
    // Abre el modal para agregar un nuevo campo
    setModalOpenCampos(true);
    setTipoCampo({ ...tipoCampo, tipo: tipoCampo });
    setSectionIndex(sectionIndex);
    setArrayCampoOptions([]);
    setArrayFilasOption([]);
    setArrayColumnasOption([]);
  };
  const handleClick = () => {
    const insertAt = 1; // Podría ser cualquier índice
    const nextArtists = [
      // Elementos antes del punto de inserción:
      ...arrayCampoOptions.slice(0, insertAt),
      // Nuevo ítem:
      { valor: valorTotal, etiqueta: campoOptions },
      // Elementos después del punto de inserción:
      ...arrayCampoOptions.slice(insertAt),
    ];
    setArrayCampoOptions(nextArtists);
    setCampoOptions("");
    setValorTotal((prevValor) => prevValor + 1);
  };
  const handleClickFilas = () => {
    const insertAt = 1; // Podría ser cualquier índice
    const nextArtists = [
      // Elementos antes del punto de inserción:
      ...arrayFilasOption.slice(0, insertAt),
      // Nuevo ítem:
      { valor: valorTotal, etiqueta: filasOption },
      // Elementos después del punto de inserción:
      ...arrayFilasOption.slice(insertAt),
    ];
    setArrayFilasOption(nextArtists);
    setFilasOption("");
    setValorTotal((prevValor) => prevValor + 1);
  };
  const handleClickColumnas = () => {
    const insertAt = 1; // Podría ser cualquier índice
    const nextArtists = [
      // Elementos antes del punto de inserción:
      ...arrayColumnasOption.slice(0, insertAt),
      // Nuevo ítem:
      { valor: valorTotal, etiqueta: columnasOption },
      // Elementos después del punto de inserción:
      ...arrayColumnasOption.slice(insertAt),
    ];
    setArrayColumnasOption(nextArtists);
    setColumnasOption("");
    setValorTotal((prevValor) => prevValor + 1);
  };

  const handleGuardarCampo = async () => {
    const updatedFormState = { ...formState };
    let nuevaPregunta;
    // Verificar que formState.sesiones no sea undefined
    if (
      !updatedFormState.sesiones ||
      !updatedFormState.sesiones[sectionIndex]
    ) {
      console.error(
        "No se pudo encontrar la sesión correspondiente en formState.sesiones"
      );
      return;
    }
    switch (tipoCampo.tipo) {
      // Casos para tipos específicos...
      case "texto":
      case "correo":
      case "decimal":
      case "numerico":
      case "fecha":
      case "hora":
      case "textarea":
        if (
          !tipoCampo.etiqueta ||
          !tipoCampo.nombre ||
          !tipoCampo.tipo ||
          !tipoCampo.placeholder ||
          !tipoCampo.nombre ||
          !tipoCampo.requerido
        ) {
          show_alerta("todos los campos son obligatorios", "warning");
          return;
        }
        nuevaPregunta = {
          tipo: tipoCampo.tipo,
          etiqueta: tipoCampo.etiqueta,
          nombre: tipoCampo.nombre,
          placeholder: tipoCampo.placeholder,
          requerido: tipoCampo.requerido,
          opciones: [],
        };
        break;
      case "gps":
        if (
          !tipoCampo.etiqueta ||
          !tipoCampo.tipo ||
          !tipoCampo.placeholder ||
          !tipoCampo.requerido
        ) {
          show_alerta("todos los campos son obligatorios", "warning");
          return;
        }
        nuevaPregunta = {
          tipo: tipoCampo.tipo,
          etiqueta: tipoCampo.etiqueta,
          nombre: tipoCampo.nombre,
          placeholder: tipoCampo.placeholder,
          requerido: tipoCampo.requerido,
          opciones: [],
        };
        break;
      case "matriz":
        if (
          !tipoCampo.etiqueta ||
          !tipoCampo.nombre ||
          !tipoCampo.tipo ||
          !tipoCampo.placeholder ||
          !tipoCampo.nombre ||
          !tipoCampo.requerido ||
          !arrayFilasOption.length ||
          !arrayColumnasOption.length
        ) {
          show_alerta("todos los campos son obligatorios", "warning");
          return;
        }
        nuevaPregunta = {
          tipo: tipoCampo.tipo,
          etiqueta: tipoCampo.etiqueta,
          nombre: tipoCampo.nombre,
          placeholder: tipoCampo.placeholder,
          requerido: tipoCampo.requerido,
          filas: arrayFilasOption,
          columnas: arrayColumnasOption,
        };
        break;

      case "checkbox":
      case "lista":
      case "opcion_unica":
        if (
          !tipoCampo.etiqueta ||
          !tipoCampo.nombre ||
          !tipoCampo.tipo ||
          !tipoCampo.nombre ||
          !tipoCampo.requerido ||
          !arrayCampoOptions.length
        ) {
          show_alerta("todos los campos son obligatorios", "warning");
          return;
        }
        nuevaPregunta = {
          tipo: tipoCampo.tipo,
          etiqueta: tipoCampo.etiqueta,
          nombre: tipoCampo.nombre,
          requerido: tipoCampo.requerido,
          opciones: arrayCampoOptions,
        };
        break;

      // Otros casos según los tipos de campo que tengas...

      default:
        console.error("Tipo de campo no reconocido:", tipoCampo.tipo);
        return;
    }

    if (!updatedFormState.sesiones[sectionIndex].campos) {
      updatedFormState.sesiones[sectionIndex].campos = []; // Inicializar si no está definido
    }

    // Verificar si se está creando un nuevo campo o actualizando uno existente
    if (preguntaQuiestion.tipo === tipoCampo.tipo) {
      // Actualizar campo existente
      const existingCampos = updatedFormState.sesiones[sectionIndex].campos;
      const existingIndex = existingCampos.findIndex(
        (campo) => campo.nombre === nuevaPregunta.nombre
      );

      if (existingIndex !== -1) {
        // Si ya existe un campo con el mismo nombre, lo reemplazamos
        existingCampos[existingIndex] = nuevaPregunta;
      } else {
        // Si no existe un campo con el mismo nombre, lo agregamos
        existingCampos.push(nuevaPregunta);
      }
    } else {
      // Crear nuevo campo
      updatedFormState.sesiones[sectionIndex].campos.push(nuevaPregunta);
    }
    setFormState(updatedFormState);
    setModalOpenCampos(false);
    setTipoCampo({
      tipo: "",
      etiqueta: "",
      placeholder: "",
      nombre: "",
      requerido: "true",
      opciones: [],
    });
  };
  const actualizarCampo = async (sectionIndex, questionIndex) => {
    const updatedFormState = { ...formState };
    const camposActual = {
      ...updatedFormState.sesiones[sectionIndex].campos[questionIndex],
    };
    setModalOpenCampos(true);
    setSectionIndex(sectionIndex);
    setPreguntaQuiestion({
      tipo: camposActual.tipo,
    });
    setArrayCampoOptions(camposActual.opciones);
    setArrayFilasOption(camposActual.filas);
    setArrayColumnasOption(camposActual.columnas);
    setTipoCampo({
      tipo: camposActual.tipo,
      etiqueta: camposActual.etiqueta,
      placeholder: camposActual.placeholder,
      nombre: camposActual.nombre,
      requerido: camposActual.requerido,
      opciones: camposActual.opciones || [],
      columnas: camposActual.columnas || [],
      filas: camposActual.filas || [],
    });
  };

  const toggleCampos = () => {
    setShowCampos(!showCampos);
  };

  const toggleSection = (sectionIndex) => {
    const updatedIsOpenState = [...isOpenState];
    updatedIsOpenState[sectionIndex] = !updatedIsOpenState[sectionIndex];
    setIsOpenState(updatedIsOpenState);
  };

  const renderCampo = (campo, questionIndex) => {
    if (!campo) {
      return null;
    }

    switch (campo.tipo) {
      case "texto":
      case "correo":
        return (
          <div>
            <input
              key={questionIndex}
              type={campo.tipo}
              placeholder={campo.placeholder}
              required={campo.requerido}
              disabled
            />
          </div>
        );
      case "lista":
        return (
          <div key={questionIndex}>
            <select className="selectform">
              {campo.opciones &&
                campo.opciones.map((opcion, index) => (
                  <option key={index} value={opcion.valor}>
                    {opcion.etiqueta}
                  </option>
                ))}
            </select>
          </div>
        );
      case "matriz":
        const filas = campo.filas;
        const columnas = campo.columnas;

        if (!filas || !columnas) {
          return null;
        }

        return (
          <table key={questionIndex} className="matrix-table">
            <thead>
              <tr>
                <th></th>
                {columnas.map((columna, columnIndex) => (
                  <th key={columnIndex}>{columna.etiqueta}</th>
                ))}
              </tr>
            </thead>
            <tbody>
              {filas.map((fila, filaIndex) => (
                <tr key={filaIndex}>
                  <td>{fila.etiqueta}</td>
                  {columnas.map((columna, columnIndex) => (
                    <td key={columnIndex}>
                      <input
                        type="radio"
                        name={`respuesta_fila_${filaIndex}`}
                        value={`Respuesta para ${fila.etiqueta} - ${columna.etiqueta}`}
                      />
                    </td>
                  ))}
                </tr>
              ))}
            </tbody>
          </table>
        );

      case "decimal":
        return (
          <input
            key={questionIndex}
            type="number" // Use type "text" for decimal input
            pattern="[0-9]*[.]?[0-9]*" // Allow only valid decimal values
            placeholder={campo.placeholder}
            required={campo.requerido}
          />
        );

      case "numerico":
        return (
          <input
            key={questionIndex}
            type="number" // Use type "number" for numeric input
            pattern="[0-9]*" // Allow only valid integer values
            placeholder={campo.placeholder}
            required={campo.requerido}
          />
        );

      case "checkbox":
        return (
          <div key={questionIndex} className="form-question">
            {campo.opciones &&
              campo.opciones.map((opcion, index) => (
                <label key={index}>
                  <input
                    className="input"
                    type="checkbox"
                    value={opcion.valor}
                  />
                  {opcion.etiqueta}
                </label>
              ))}
          </div>
        );
      case "opcion_unica":
        return (
          <div key={questionIndex} className="form-question">
            {campo.opciones &&
              campo.opciones.map((opcion, index) => (
                <div key={index}>
                  <input className="input" type="radio" value={opcion.valor} />
                  <span>{opcion.etiqueta}</span>
                </div>
              ))}
          </div>
        );
      case "fecha":
        return (
          <DatePicker
            key={questionIndex}
            selected={startDate} // Usa un estado para manejar la fecha seleccionada
            onChange={(date) => setStartDate(date)}
            placeholderText={campo.placeholder}
            required={campo.requerido}
          />
        );
      case "hora":
        return (
          <TimePicker
            key={questionIndex}
            value={hora} // Usa un estado para manejar la hora seleccionada
            onChange={(time) => setHora(time)}
            required={campo.requerido}
          />
        );
      case "textarea":
        return (
          <textarea
            className="textarea"
            key={questionIndex}
            placeholder={campo.placeholder}
            required={campo.requerido}
          />
        );
      case "foto":
      case "audio":
      case "gps":
        return (
          <ContentIcon
            contentType={campo.tipo}
            placeholder={campo.placeholder}
          />
        );
      default:
        return null;
    }
  };

  const logFormData = async () => {
    try {
      const update = await formControler.handleUpdateForm(
        idformulario,
        formState
      );
      if (update) {
        show_alerta("formulario actualizado exitosamente ", "success");
        navigate(`/guias`);
      } else {
        show_alerta("error en actualizar el formulario ", "error");
      }
    } catch (error) {
      show_alerta(
        "Error al actualizar el formulario. Por favor, inténtalo de nuevo.",
        "error"
      );
    }
  };
  return (
    <div className="container-body-carac">
      <div className="container-body-button">
        <Link className="link" to="/guias">
          <button className="btns_s" data-tooltip="ATRAS">
            <BsXCircleFill />
          </button>
        </Link>
      </div>
      <h1>{formState.nombreFormulario}</h1>
      <h2>{formState.descripcionFormulario}</h2>
      <div className="form-container-guia">
        {formState.sesiones.map((seccion, sectionIndex) => (
          <div
            key={sectionIndex}
            ref={(ref) =>
              focusedSection === seccion.nombre && ref && ref.focus()
            }
            className="form-section-guia"
          >
            <div
              className="section-header-guia"
              onClick={() => toggleSection(sectionIndex)}
            >
              <div className="text-header">
                <p style={{ margin: "10px" }}>{seccion.nombre} </p>
                <div className="buttons-header">
                  <button
                    className="headers_btn"
                    onClick={() => modificarSesion(sectionIndex)}
                    data-tooltip="Modificar"
                  >
                    <FaSync />
                  </button>
                  <button
                    className="headers_btn"
                    onClick={() => removeSesion(sectionIndex)}
                    data-tooltip="Eliminar"
                  >
                    <BsTrash2 />
                  </button>
                  <span style={{ margin: "10px" }}>
                    {isOpenState[sectionIndex] ? (
                      <FaAngleDown />
                    ) : (
                      <FaAngleRight />
                    )}
                  </span>
                </div>
              </div>
            </div>
            {isOpenState[sectionIndex] && (
              <div className="form-questions">
                {seccion.campos.map((question, questionIndex) => (
                  <div key={questionIndex} className="form-question">
                    <div
                      className="questions"
                      style={{ display: "flex", alignItems: "center" }}
                    >
                      <p>{question.etiqueta}:</p>
                      <div className="btns" style={{ marginLeft: "10px" }}>
                        <button
                          onClick={() =>
                            eliminarCampo(sectionIndex, questionIndex)
                          }
                        >
                          Eliminar
                        </button>
                        <button
                          onClick={() =>
                            actualizarCampo(sectionIndex, questionIndex)
                          }
                          style={{ marginLeft: "5px" }}
                        >
                          Actualizar
                        </button>
                      </div>
                    </div>
                    {renderCampo(question, questionIndex)}
                  </div>
                ))}

                <button
                  className="btns_s"
                  onClick={() => toggleCampos()}
                  data-tooltip="Tipos de Campos"
                >
                  <BsListTask />
                </button>
                {showCampos && (
                  <div className="campos-container">
                    <p>Tipos de Campos</p>
                    <div className="campos-buttons">
                      {tiposDeCampos.map((campo, index) => (
                        <button
                          key={index}
                          className="campo-button"
                          onClick={() => {
                            agregarCampo(campo.tipo, sectionIndex); // Abrir la modal al seleccionar un tipo de campo
                          }}
                        >
                          {campo.icono} {campo.etiqueta}
                        </button>
                      ))}
                    </div>
                  </div>
                )}
              </div>
            )}
          </div>
        ))}
        <div className="form-section">
          <button
            className="btns_s"
            onClick={() => addSesion()}
            data-tooltip="Agregar Sesión"
          >
            <BsFillPlusCircleFill />
          </button>

          <ModalRegistroGuia
            isOpen={modalOpenSesion}
            onRequestClose={handleModalClose}
            nombreSesion={nombreSesion}
            descripcionSesion={descripcionSesion}
            setNombreSesion={setNombreSesion}
            setDescripcionSesion={setDescripcionSesion}
            handleGuardarSesion={handleGuardarSesion}
            handleModalClose={handleModalClose}
          />
        </div>
        <div>
          <Modal
            isOpen={modalOpenCampos}
            onClose={handleModalClose}
            style={{
              overlay: {
                backgroundColor: "rgba(255, 255, 255, 0)", // Fondo transparente
              },
              content: {
                maxWidth: "800px",
                width: "90%", // Ancho del modal
                maxHeight: "63%",
                height: "auto", // Alto del modal
                margin: "auto", // Centrado horizontal
                padding: "10px", // Espacio alrededor del contenido
                backgroundColor: "white", // Color de fondo del modal
                overflowY: "auto", // Agregar scroll vertical si es necesario
                border: "none", // Eliminar borde
                borderRadius: "none", // Eliminar bordes redondeados
                boxShadow: "none", // Eliminar sombra
              },
            }}
          >
            <div className="Modal">
              <h1>Tipo: {tipoCampo.tipo}</h1>
              <div className="Modal-tipos">
                <div style={{ flex: 1 }}>
                  <h2>Ingrese el título de la pregunta</h2>
                  <input
                    type="text"
                    value={tipoCampo.etiqueta}
                    onChange={(e) =>
                      setTipoCampo({
                        ...tipoCampo,
                        etiqueta: e.target.value,
                      })
                    }
                  />
                </div>
                {tipoCampo.tipo !== "checkbox" &&
                  tipoCampo.tipo !== "opcion_unica" &&
                  tipoCampo.tipo !== "lista" && (
                    <div style={{ flex: 1 }}>
                      <h2>Ingrese la descripción de la pregunta</h2>
                      <input
                        type="text"
                        value={tipoCampo.placeholder}
                        onChange={(e) =>
                          setTipoCampo({
                            ...tipoCampo,
                            placeholder: e.target.value,
                          })
                        }
                      />
                    </div>
                  )}
              </div>
              <div className="Modal-tipos">
                <div style={{ flex: 1 }}>
                  <h2>Ingrese el nombre por debajo</h2>
                  <input
                    type="text"
                    value={tipoCampo.tipo === "gps" ? "ubicacion" : tipoCampo.nombre}
                    onChange={(e) =>
                      setTipoCampo({
                        ...tipoCampo,
                        nombre: tipoCampo.tipo === "gps" ? "ubicacion" : e.target.value,
                      })
                    }
                    disabled={tipoCampo.tipo === "gps"}
                  />
                </div>
                <div style={{ flex: 1 }}>
                  <h2>Ingrese si es requerido o no</h2>
                  <input
                    type="text"
                    value={tipoCampo.requerido}
                    disabled
                    onChange={(e) =>
                      setTipoCampo({
                        ...tipoCampo,
                        requerido: e.target.value,
                      })
                    }
                  />
                  <button
                    style={{
                      backgroundColor:
                        tipoCampo.requerido === "true" ? "#009940" : "#4886C6",
                      color: "white",
                      padding: "10px",
                      borderRadius: "5px",
                    }}
                    onClick={() =>
                      setTipoCampo({
                        ...tipoCampo,
                        requerido:
                          tipoCampo.requerido === "true" ? "false" : "true",
                      })
                    }
                  >
                    {tipoCampo.requerido === "true"
                      ? "Obligatorio"
                      : "No Obligatorio"}
                  </button>
                </div>
              </div>
              {tipoCampo.tipo === "checkbox" ||
              tipoCampo.tipo === "opcion_unica" ||
              tipoCampo.tipo === "lista" ? (
                <div className="Modal-tipos">
                  <div style={{ flex: 1 }}>
                    <h2>Ingrese las opciones</h2>
                    <input
                      value={campoOptions}
                      onChange={(e) => setCampoOptions(e.target.value)}
                    />
                    <button className="btn_op" onClick={handleClick}>
                      Insertar
                    </button>
                    <ul>
                      <div className="lista-horizontal">
                        {arrayCampoOptions.map((campo) => (
                          <li key={campo.valor}>
                            {campo.etiqueta}
                            <button
                              className="btn_op"
                              onClick={() => {
                                setArrayCampoOptions(
                                  arrayCampoOptions.filter(
                                    (a) => a.etiqueta !== campo.etiqueta
                                  )
                                );
                              }}
                            >
                              Eliminar
                            </button>
                          </li>
                        ))}
                      </div>
                    </ul>
                  </div>
                </div>
              ) : null}

              {tipoCampo.tipo === "matriz" && (
                <div className="Modal-tipos">
                  <div style={{ flex: 1 }}>
                    <h2>Ingrese las fila</h2>
                    <input
                      value={filasOption}
                      onChange={(e) => setFilasOption(e.target.value)}
                    />
                    <button className="btn_op" onClick={handleClickFilas}>
                      Insertar
                    </button>
                    <ul>
                      <div className="lista-horizontal">
                        {arrayFilasOption.map((filas) => (
                          <li key={filas.valor}>
                            {filas.etiqueta}
                            <button
                              className="btn_op"
                              onClick={() => {
                                setArrayFilasOption(
                                  arrayFilasOption.filter(
                                    (a) => a.etiqueta !== filas.etiqueta
                                  )
                                );
                              }}
                            >
                              Eliminar
                            </button>
                          </li>
                        ))}
                      </div>
                    </ul>
                  </div>
                  <div style={{ flex: 1 }}>
                    <h2>Ingrese las columnas</h2>
                    <input
                      value={columnasOption}
                      onChange={(e) => setColumnasOption(e.target.value)}
                    />
                    <button className="btn_op" onClick={handleClickColumnas}>
                      Insertar
                    </button>
                    <ul>
                      <div className="lista-horizontal">
                        {arrayColumnasOption.map((columnas) => (
                          <li key={columnas.valor}>
                            {columnas.etiqueta}
                            <button
                              className="btn_op"
                              onClick={() => {
                                setArrayColumnasOption(
                                  arrayColumnasOption.filter(
                                    (a) => a.etiqueta !== columnas.etiqueta
                                  )
                                );
                              }}
                            >
                              Eliminar
                            </button>
                          </li>
                        ))}
                      </div>
                    </ul>
                  </div>
                </div>
              )}
              {tipoCampo.etiqueta}
              {renderCampo(tipoCampo, 0)}
              <div className="btn_td">
                <button className="save_btn" onClick={handleGuardarCampo}>
                  Guardar Campo
                </button>
                <button className="cancel_btn" onClick={handleModalClose}>
                  Cancelar
                </button>
              </div>
            </div>
          </Modal>
        </div>
      </div>
      <div className="log-form-data">
        <button className="btns_ms" onClick={logFormData}>
          <span style={{ marginRight: "8px" }}>
            <FaSync />
          </span>
          Modificar Formulario
        </button>
      </div>
    </div>
  );
};

export default GuiasCaract;
