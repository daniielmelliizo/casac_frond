import React from "react";
import Modal from "react-modal";
import GuiasListCheck from "./GuiasListCheck";
import "../../../styles/Despliegues.css";
Modal.setAppElement("#root");

const Despliegues = () => {
  
  return (
    <div className="content-despliegue">
      <div className="content2">
        <h3>Despliegue y comparación de guías </h3>
        <GuiasListCheck/>
      </div>

    </div>
  );
};

export default Despliegues;
