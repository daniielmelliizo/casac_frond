import React, { useState, useEffect } from "react";
import { FaCheck, FaSearch, FaEye } from "react-icons/fa";
import { MdAdd, MdCompare } from "react-icons/md";
import "../../../styles/GuiasListCheck.css";
import { useNavigate } from "react-router-dom";
import GuiaService from "../../../../application/services/guiaService";
import GuiaController from "../../../../interfaz/controllers/guiaController";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { show_alerta } from "../../../../function";
import DespliegueService from "../../../../application/services/despliegueService";
import DespliegueController from "../../../../interfaz/controllers/despleguieController";
import SeccionService from "../../../../application/services/sectionService";
import SeccionesController from "../../../../interfaz/controllers/seccionesController";
import { ColorRing } from 'react-loader-spinner'

const guiaService = new GuiaService();
const guiaController = new GuiaController(guiaService);
const despliegueService = new DespliegueService();
const despliegueController = new DespliegueController(despliegueService);
const seccionesService = new SeccionService();
const seccionesController = new SeccionesController(seccionesService);

const GuiasListCheck = () => {
  const navigate = useNavigate();
  const [guias, setGuias] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [currentPage] = useState(1);
  const [perPage] = useState(10);
  const [searchTerm, setSearchTerm] = useState("");
  const [searchTermDespliegue, setSearchTermDespliegue] = useState("");
  const [selectedCards, setSelectedCards] = useState([]);


  const indexOfLastPost = currentPage * perPage;
  const indexOfFirstPost = indexOfLastPost - perPage;
  const currentPosts = guias.filter((guia) => {
    const fullName = `${guia.nombreFormulario}`.toLowerCase();
    const fullDespliegue = `${guia.status}`.toLowerCase();
    guias.slice(indexOfFirstPost, indexOfLastPost);
    return (
      fullName.includes(searchTerm.toLowerCase()) &&
      fullDespliegue.includes(searchTermDespliegue.toLowerCase())
    );
  });

  const handleGetGuias = async () => {
    try {
      const response = await guiaController.handleGetGuias();
      const guiasData = response.formularios || []; // Extract the array from the object
      setGuias(guiasData);
      setLoading(false);
    } catch (error) {
      setError(error.message);
      setLoading(false);
    }
  };

  const handleSearch = (e) => {
    setSearchTerm(e.target.value);
  };
  const handleSearchDespliegue = (e) => {
    setSearchTermDespliegue(e.target.value);
    handleGetGuias();
  };
  const handleCardClick = (guiaId) => {
    const isSelected = selectedCards.includes(guiaId);
    setSelectedCards((prevSelected) =>
      isSelected
        ? prevSelected.filter((id) => id !== guiaId)
        : [...prevSelected, guiaId]
    );
    handleGetGuias();
  }; 
  

  useEffect(() => {
    const handleGetGuias = async () => {
      try {
        const response = await guiaController.handleGetGuias();
        const guiasData = response.formularios || []; // Extract the array from the object
        setGuias(guiasData);
        setLoading(false);
      } catch (error) {
        setError(error.message);
        setLoading(false);
      }
    };

    handleGetGuias();
  }, []);
  //comparamaos las lista si tienen preguntas similares
  const handleCompare = async () => {
    if (selectedCards.length === 2) {
      // Almacena las respuestas de cada guía
      const respuestasGuia1 = await seccionesController.handleBuscarSeccion(
        selectedCards[0]
      );
      const respuestasGuia2 = await seccionesController.handleBuscarSeccion(
        selectedCards[1]
      );

      // Verifica que ambas respuestas sean válidas
      if (respuestasGuia1 && respuestasGuia2) {
        const preguntasSimilares = [];

        // Itera sobre las sesiones y compara pregunta por pregunta
        for (let i = 0; i < respuestasGuia1.sesiones.length; i++) {
          const sesion1 = respuestasGuia1.sesiones[i]["0"].sesion;
          const sesion2 = respuestasGuia2.sesiones[i]["0"].sesion;

          // Verifica si 'preguntas' está definido
          if (sesion1 && sesion2 && sesion1.preguntas && sesion2.preguntas) {
            // Itera sobre las preguntas y compara los títulos
            for (let j = 0; j < sesion1.preguntas.length; j++) {
              const pregunta1 = sesion1.preguntas[j];
              const pregunta2 = sesion2.preguntas[j];

              // Verifica si ambas preguntas son válidas y tienen el mismo título
              if (
                pregunta1 &&
                pregunta2 &&
                pregunta1.titulo_pregunta &&
                pregunta2.titulo_pregunta
              ) {
                if (pregunta1.titulo_pregunta === pregunta2.titulo_pregunta) {
                  preguntasSimilares.push(pregunta1.titulo_pregunta);
                }
              } else {
                console.error(
                  `Estructura de pregunta no válida en la sesión ${i + 1
                  }, pregunta ${j + 1}`
                );
              }
            }
          } else {
            console.error("La estructura de las sesiones no es la esperada.");
          }
        }

        // Muestra una alerta con información sobre preguntas similares
        if (preguntasSimilares.length > 0) {
          show_alerta(
            `Preguntas similares encontradas: ${preguntasSimilares.join(", ")}`,
            "info"
          );
        } else {
          show_alerta(
            "No se encontraron preguntas similares entre las guías.",
            "info"
          );
        }
      } else {
        show_alerta(
          "No se pudieron obtener las respuestas para alguna de las guías.",
          "error"
        );
      }
    } else {
      show_alerta("Selecciona exactamente dos guías para comparar.", "info");
    }
  };

  //desplegar las listas check
  const handleDesplegar = async () => {
    try {
      const MySwal = withReactContent(Swal);
      MySwal.fire({
        title: "Seguro de desplegar estas guias ?",
        icon: "question",
        text: "no hay marcha atrás",
        showCancelButton: true,
        confirmButtonText: "Despliegue",
        cancelButtonText: "Cancelar",
      }).then(async (result) => {
        if (result.isConfirmed) {
          const dataToSend = {ids:selectedCards}
          const respuesta =
           await despliegueController.handleUpdateDespliegue(dataToSend);
          if (respuesta) {
            show_alerta("Despleguie exitosamente ", "success");
          await  handleGetGuias();
          } else {
            show_alerta(
              "Error al desplegar las guías. Por favor, inténtalo de nuevo.",
              "error"
            );
          }
        } 
      });
    }catch (error) {
      show_alerta("Error al desplegar la guia", "error");
    }
  };

  const handleNavigate = (id) => {
    // Navega a la ruta `/caracterizacion` con el id en el estado
    navigate("/PrevisualForm", { state: { idform: id } });
  };

  if (loading) {
    return <div><ColorRing
      visible={true}
      height="80"
      width="80"
      ariaLabel="blocks-loading"
      wrapperStyle={{}}
      wrapperClass="blocks-wrapper"
      colors={['#e15b64', '#f47e60', '#f8b26a', '#abbd81', '#849b87']}
    /> <h3>cargando ....</h3> </div>;
  }

  if (error) {
    return <p>Error: {error}</p>;
  }

  if (!Array.isArray(guias)) {
    return <p>Error: Guias is not an array</p>;
  }

  return (
    <div className="container-full">
      <div className="search">
        <div className="search-barra">
          {/* <FaSearch className="icon-search" /> */}
          <input
            type="text"
            value={searchTerm}
            onChange={handleSearch}
            placeholder="Buscar..."
          />

          <button className="actions" onClick={handleCompare}>
            <MdCompare size={20} />
            <span>Comparar Guías</span>
          </button>
          <button className="actions" onClick={handleDesplegar}>
            <MdAdd size={20} />
            <span>Desplegar Guías</span>
          </button>

          <p>Ordenar por</p>
          <select
            id="selectOption"
            value={searchTermDespliegue}
            onChange={handleSearchDespliegue}
          >
            <option value="">Selecciona...</option>
            <option value="activo">Despliegue</option>
            <option value="inactivo">Sin Desplegar</option>
          </select>
        </div>
      </div>
      <div
        className="guias-desp-container"
        handleCardClick={handleCardClick}
        selectedCards={selectedCards}

      >
        {currentPosts.map((guia) => (
          <div
            key={guia.id}
            className="contenido"
            style={{
              backgroundColor: guia.status === 'activo' ? '#9CC430' : '',
              border: selectedCards.includes(guia.id) ? '5px solid #009940' : '',
            }}
            onClick={() => handleCardClick(guia.id)}
          >
            <div className="guia-cabecera">
              <h1>{guia.nombreFormulario}</h1>
              <div
                className={`icon-check ${selectedCards.includes(guia.id) ? "selected" : ""
                  }`}
              >
                <FaCheck />
              </div>
              <div
                className={`icon ${selectedCards.includes(guia.id) ? "selected" : ""
                  }`}
              >
                <div className="link" onClick={() => handleNavigate(guia.id)}>
                  <FaEye />
                </div>
              </div>
            </div>
            <p>{`Descripcion: ${guia.descripcionFormulario}`}</p>
            <p>{`Creado el: ${guia.fechaCreacion}`}</p>
            <p>{`Última modificación: ${guia.fechaActualizacion}`}</p>
          </div>
        ))}

      </div>
    </div>
  );
};

export default GuiasListCheck;
