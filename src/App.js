import React, { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate,
  Outlet,
} from "react-router-dom";
import AuthenticationService from "./application/services/authService";
import AuthenticationController from "./interfaz/controllers/authController";
import LoginForm from "./ui/pages/public/LoginForm";
import Home from "./ui/pages/private/home/Home";
import Profile from "./ui/pages/private/home/Profile";
import Guias from "./ui/pages/private/guides/Guias";
import GuiasCaract from "./ui/pages/private/guides/GuiasCarac";
import Despliegues from "./ui/pages/private/deployment/Despliegues";
import Reportes from "./ui/pages/private/reports/Reportes";
import GestionUsuarios from "./ui/pages/private/users/GestionUsuarios";
import PrevisualForm from "./ui/pages/private/guides/PrevisualForm";
import Navbar from "./ui/components/navigate/Navbar";
import "./App.css";
import { show_alerta } from "../src/function";
import Region from "./ui/pages/private/reports/region/Region";
import Reporte from "./ui/pages/private/reports/finca/Finca";
import Header from "./ui/components/navigate/SectionHeader";

const authenticationService = new AuthenticationService();
const authenticationController = new AuthenticationController(
  authenticationService
);

const App = () => {
  // Verifica si hay información de inicio de sesión en el almacenamiento local al cargar la aplicación
  const initialLoggedInState = localStorage.getItem("isLoggedIn") === "true";
  const [isLoggedIn, setIsLoggedIn] = useState(initialLoggedInState);
  const [showHeader, setShowHeader] = useState(true);

  const handleLogin = async (email, password) => {
    // Lógica para autenticar al usuario
    if ((await authenticationController.handleLogin(email, password)) === true) {
      setIsLoggedIn(true);
      // Guarda el estado de inicio de sesión en el almacenamiento local
      localStorage.setItem("isLoggedIn", "true");
    } else {
      show_alerta("Credenciales incorrectas intentelo de nuevo.", "error");
    }
  };

  const handleLogout = () => {
    // Lógica para cerrar sesión
    setIsLoggedIn(false);
    // Elimina la información de inicio de sesión del almacenamiento local
    localStorage.clear();
};


  useEffect(() => {
    const handleScroll = () => {
      const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
      if (scrollTop > 100) {
        setShowHeader(false);
      } else {
        setShowHeader(true);
      }
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const PrivateRoute = ({ element, ...props }) => {
    return isLoggedIn ? element : <Navigate to="/" />;
  };

  return (
    <Router>
      <div className="pag">
        {showHeader && isLoggedIn && <Header onLogout={handleLogout} />}
        <div className="content-complet">
          <div className="Nav">{isLoggedIn && <Navbar onLogout={handleLogout} />}</div>
          <div className="paginas">
            <Routes>
              <Route
                path="/"
                element={
                  isLoggedIn ? <Outlet /> : <LoginForm onLogin={handleLogin} />
                }
              />
              <Route path="/home" element={isLoggedIn ? <Home /> : <Navigate to="/" />} />
              <Route path="/guias" element={<PrivateRoute element={<Guias />} />} />
              <Route path="/despliegues" element={<PrivateRoute element={<Despliegues />} />} />
              <Route path="/reportes" element={<PrivateRoute element={<Reportes />} />} />
              <Route
                path="/gestion-usuarios"
                element={<PrivateRoute element={<GestionUsuarios />} />}
              />
              <Route
                path="/caracterizacion"
                element={<PrivateRoute element={<GuiasCaract />} />}
              />
              <Route
                path="/PrevisualForm"
                element={<PrivateRoute element={<PrevisualForm />} />}
              />
              <Route
                path="/Profile"
                element={<PrivateRoute element={<Profile />} />}
              />
              <Route path="/Finca" element={<PrivateRoute element={<Reporte />} />} />
              <Route path="/Region" element={<PrivateRoute element={<Region />} />} />
            </Routes>
          </div>
        </div>
      </div>
    </Router>
  );
};

export default App;
