import axios from '../infraestructura/axiosConfig';

export const requests = async (metodo, endpoint, datos = null) => {
  try {
    let response;

    switch (metodo.toUpperCase()) {
      case 'GET':
        response = await axios.get(endpoint);
        break;
      case 'POST':
        response = await axios.post(endpoint, datos);
        break;
      case 'PUT':
        response = await axios.put(endpoint, datos);
        break;
      case 'DELETE':
        response = await axios.delete(endpoint);
        break;
      default:
        throw new Error('Método HTTP no válido');
    }

    return response.data;
  } catch (error) {
    throw error;
  }
};

