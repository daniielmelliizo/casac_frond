import axios from 'axios';

const apiClient = axios.create({
  baseURL: 'https://casac-com.onrender.com', // Reemplaza con la URL de tu API
  timeout: 5000,
});

export default apiClient;