import { requests } from "../../adapters/ServiciosAdapter";

class GuiaService {
  async createGuia(data) {
    try {
      const guia = await requests("POST", "/form/create", data);
      // console.log(guia.status);
      return guia;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
      throw new Error("Error de creacion guia");
    }
  }
  async getGuia() {
    try {
      const guia = await requests("GET", "/form/listar");
      // console.log(guia);
      return guia;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
      throw new Error("Error de creacion guia");
    }
  }

  async updateGuia(id, data) {
    try {
      const guia = await requests("PUT", `/guia/update_guia/${id}`, data);
      // console.log(guia.status);
      return guia;
    } catch (error) {
      throw new Error("Error al actualizar guía");
    }
  }

  async deleteGuia(id) {
    try {
      const result = await requests("DELETE", `/form/eliminar/${id}`);
      // console.log(result.status);
      return result;
    } catch (error) {
      throw new Error("Error al eliminar guía");
    }
  }
  async getGuiaConsult(id) {
    try {
      const result = await requests("GET", `/guia/consultar/${id}`);
      return result;
    } catch (error) {
      throw new Error("Error al consultar guía");
    }
  }

  async getFormActivo() {
    try {
      const result = await requests("GET", "/form/listarActivos");
      // console.log(result);
      return result;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
      throw new Error("Error de extracion del formulario");
    }
  }
}
export default GuiaService;
