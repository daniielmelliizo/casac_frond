import { requests } from "../../adapters/ServiciosAdapter";

class DespliegueService {

    async updateDespliegue(data) {
        try {
          const despliegue = await requests("PUT", `/sync/desplegar`, data);
          // console.log(despliegue.status);
          return despliegue;
        } catch (error) {
          throw new Error("Error al despliegar la guía");
        }
      }
}
export default DespliegueService;