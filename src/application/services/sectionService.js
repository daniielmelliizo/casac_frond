import { requests } from "../../adapters/ServiciosAdapter";

class SeccionService {
  async createSeccion(data) {
    try {
      const seccion = await requests("POST", "/sesiones/create_sesiones", data);

      return seccion;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
      throw new Error("Error de creacion guia");
    }
  }

  async buscarSeccion(data) {
    try {
      const seccion = await requests("GET", `/sesiones/search_sesions/${data}`);
      return seccion;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
      throw new Error("Error de creacion guia");
    }
  }

  async updateSeccion(id, data) {
    try {
      const seccion = await requests(
        "PUT",
        `/sesiones/edit_sesions/${id}`,
        data
      );

      return seccion;
    } catch (error) {
      throw new Error("Error de actualizar la sección");
    }
  }
}
export default SeccionService;
