import { requests } from "../../adapters/ServiciosAdapter";

class UserService {
  async createUser(data) {
     try {
      const user = await requests(
        "POST",
        "/users/create_usuario",
        data
      );
      console.log(user);
      return user;
     } catch (error) {
       // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
       console.log(error);
       if (error.response && error.response.status === 404) {
         // Verificar si es un error 404 y el mensaje específico
           throw new Error("El usuario ya está registrado con ese numero de cedula");
       }
       throw new Error('Error de creacion de usuario');
     }
  }

  async getUsers() {
    try {
      const user = await requests("GET", "/users/search_tod_user");
      // console.log(user);
      return user;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
      throw new Error("Error de creacion guia");
    }
  }

  async updateUser(id, data) {
    try {
      const users = await requests("PUT", `/users/edit_user/${id}`, data);
      // console.log(users.status);
      return users;
    } catch (error) {
      throw new Error("Error al actualizar usuairo");
    }
  }

  async deleteUser(id) {
    try {
      const result = await requests("DELETE", `/users/delete_user/${id}`);
      // console.log(result.status);
      return result;
    } catch (error) {
      throw new Error("Error al eliminar guía");
    }
  }
  async Parametricas(id) {
    try {
      const result = await requests("Get", `/response/reporter_usuario/${id}`);
      console.log("si esta trallendo");
      return result;
    } catch (error) {
      throw new Error("Error a la extracion de los forms");
    }
  }
}
export default UserService;
