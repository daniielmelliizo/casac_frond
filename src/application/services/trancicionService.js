import { requests } from "../../adapters/ServiciosAdapter";

class TrancicionService {
  async createTrancicion(data) {
     try {
      const trancición = await requests(
        "POST",
        "/ruta/Transicionreport",
        data
      );
      console.log(trancición);
      return trancición;
     } catch (error) {
       // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
       console.log(error);
       throw new Error('Error de transicion de usuario');
     }
  }


  async updateTrancicion(id, data) {
    try {
      const trancicion = await requests("PUT", `/ruta/modificar/${id}`, data);
      return trancicion;
    } catch (error) {
      throw new Error("Error a actualizar transicion");
    }
  }
  async getTrancicionConsult(id) {
    try {
      const result = await requests("GET", `/ruta/consultar/${id}`);
      return result;
    } catch (error) {
      throw new Error("Error al consultar transicion");
    }
  }

}
export default TrancicionService;
