import { requests } from "../../adapters/ServiciosAdapter";

class matricialesService {
  async getMatricialesFinca() {
    try {
      const response = await requests("GET", "/response/reporter_metric3");
      console.log("estraccion de las regiones",response);
      return response;
    } catch (error) {
      // La función hacerPeticion ya maneja errores, así que no es necesario añadir más detalles aquí
      throw new Error("Error de creacion guia");
    }
  }


}
export default matricialesService;
