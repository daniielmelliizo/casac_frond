import { requests } from "../../adapters/ServiciosAdapter";

class FormService {

  async updateForm(id, data) {
    try {
      const form = await requests("PUT", `/form/modificar/${id}`, data);
      // console.log(form.status);
      return form;
    } catch (error) {
      throw new Error("Error al actualizar formulario", error);
    }
  }

  async getFormConsult(id) {
    try {
      const result = await requests("GET", `/form/consultar/${id}`);
      return result;
    } catch (error) {
      throw new Error("Error al consultar formulario");
    }
  }

}
export default FormService;
